module org.refcodes.graphical {
	requires transitive java.desktop;
	requires transitive org.refcodes.component;

	exports org.refcodes.graphical;
}
