// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a background property.
 *
 * @param <T> The background's type to be accessed.
 */
public interface BackgroundAccessor<T> {

	/**
	 * Retrieves the background from the background property.
	 * 
	 * @return The background stored by the background property.
	 */
	T getBackground();

	/**
	 * Provides a mutator for a background property.
	 * 
	 * @param <T> The background's type to be accessed.
	 */
	public interface BackgroundMutator<T> {

		/**
		 * Sets the background for the background property.
		 * 
		 * @param aBackground The background to be stored by the background
		 *        property.
		 */
		void setBackground( T aBackground );
	}

	/**
	 * Provides a builder method for a background property returning the builder
	 * for applying multiple build operations.
	 *
	 * @param <T> the generic type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface BackgroundBuilder<T, B extends BackgroundBuilder<T, B>> {

		/**
		 * Sets the background for the background property.
		 * 
		 * @param aBackground The background to be stored by the background
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBackground( T aBackground );
	}

	/**
	 * Provides a background property.
	 * 
	 * @param <T> The background's type to be accessed.
	 */
	public interface BackgroundProperty<T> extends BackgroundAccessor<T>, BackgroundMutator<T> {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given background (setter) as
		 * of {@link #setBackground(Object)} and returns the very same value
		 * (getter).
		 * 
		 * @param aBackground The background to set (via
		 *        {@link #setBackground(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letBackground( T aBackground ) {
			setBackground( aBackground );
			return aBackground;
		}
	}
}
