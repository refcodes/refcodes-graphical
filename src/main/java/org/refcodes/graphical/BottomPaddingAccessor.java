// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a bottom padding property.
 */
public interface BottomPaddingAccessor {

	/**
	 * Retrieves the bottom padding from the bottom padding property.
	 * 
	 * @return The bottom padding stored by the bottom padding property.
	 */
	int getBottomPadding();

	/**
	 * Provides a mutator for a bottom padding property.
	 */
	public interface BottomPaddingMutator {

		/**
		 * Sets the bottom padding for the bottom padding property.
		 * 
		 * @param aBottomPadding The bottom padding to be stored by the bottom
		 *        padding property.
		 */
		void setBottomPadding( int aBottomPadding );
	}

	/**
	 * Provides a builder method for a bottom padding property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface BottomPaddingBuilder<B extends BottomPaddingBuilder<B>> {

		/**
		 * Sets the bottom padding for the bottom padding property.
		 * 
		 * @param aBottomPadding The bottom padding to be stored by the bottom
		 *        padding property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBottomPadding( int aBottomPadding );
	}

	/**
	 * Provides a bottom padding property.
	 */
	public interface BottomPaddingProperty extends BottomPaddingAccessor, BottomPaddingMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setBottomPadding(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aBottomPadding The integer to set (via
		 *        {@link #setBottomPadding(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letBottomPadding( int aBottomPadding ) {
			setBottomPadding( aBottomPadding );
			return aBottomPadding;
		}
	}
}
