// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * The {@link BoxBorderMode} specifies for according algorithms on how a box is
 * to be drawn.
 */
public enum BoxBorderMode {

	// @formatter:off
	NONE					( false, false, false, false),
	
	LEFT					( false, false, false,  true),
	
	BOTTOM					( false, false,  true, false),
	
	BOTTOM_LEFT				( false, false,  true,  true),
	
	RIGHT					( false,  true, false, false),
	
	RIGHT_LEFT				( false,  true, false,  true),
	
	RIGHT_BOTTOM			( false,  true,  true, false),
	
	RIGHT_BOTTOM_LEFT		( false,  true,  true,  true),
	
	TOP						(  true, false, false, false),
	
	TOP_LEFT				(  true, false, false,  true),
	
	TOP_BOTTOM				(  true, false,  true, false),
	
	TOP_BOTTOM_LEFT			(  true, false,  true,  true),
	
	TOP_RIGHT				(  true,  true, false, false),
	
	TOP_RIGHT_LEFT			(  true,  true, false,  true),
	
	TOP_RIGHT_BOTTOM		(  true,  true,  true, false),
	
	TOP_RIGHT_BOTTOM_LEFT	(  true,  true,  true,  true),
	
	ALL						(  true,  true,  true,  true);
 	// @formatter:on

	private boolean _isLeftBorder;

	private boolean _isTopBorder;

	private boolean _isRightBorder;

	private boolean _isBottomBorder;

	/**
	 * Checks if is left border.
	 *
	 * @return true, if is left border
	 */
	public boolean isLeftBorder() {
		return _isLeftBorder;
	}

	/**
	 * Checks if is top border.
	 *
	 * @return true, if is top border
	 */
	public boolean isTopBorder() {
		return _isTopBorder;
	}

	/**
	 * Checks if is right border.
	 *
	 * @return true, if is right border
	 */
	public boolean isRightBorder() {
		return _isRightBorder;
	}

	/**
	 * Checks if is bottom border.
	 *
	 * @return true, if is bottom border
	 */
	public boolean isBottomBorder() {
		return _isBottomBorder;
	}

	/**
	 * Instantiates a new box border mode.
	 *
	 * @param isTopBorder the is top border
	 * @param isRightBorder the is right border
	 * @param isBottomBorder the is bottom border
	 * @param isLeftBorder the is left border
	 */
	private BoxBorderMode( boolean isTopBorder, boolean isRightBorder, boolean isBottomBorder, boolean isLeftBorder ) {
		_isBottomBorder = isBottomBorder;
		_isLeftBorder = isLeftBorder;
		_isRightBorder = isRightBorder;
		_isTopBorder = isTopBorder;
	}

	/**
	 * From box borders.
	 *
	 * @param isTopBorder the is top border
	 * @param isRightBorder the is right border
	 * @param isBottomBorder the is bottom border
	 * @param isLeftBorder the is left border
	 * 
	 * @return the box border mode
	 */
	public static BoxBorderMode fromBoxBorders( boolean isTopBorder, boolean isRightBorder, boolean isBottomBorder, boolean isLeftBorder ) {
		for ( BoxBorderMode eMode : values() ) {
			if ( eMode.isTopBorder() == isTopBorder && eMode.isRightBorder() == isRightBorder && eMode.isBottomBorder() == isBottomBorder && eMode.isLeftBorder() == isLeftBorder ) {
				return eMode;
			}
		}
		throw new RuntimeException( "This code has a bug, it is missing the following enumeration element: Top := <" + isTopBorder + ">, right := <" + isRightBorder + ">, bottom := <" + isBottomBorder + ">, left := <" + isLeftBorder + ">" );
	}
}
