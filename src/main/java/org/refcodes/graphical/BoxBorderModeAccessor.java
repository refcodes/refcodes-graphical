// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a box border mode property.
 */
public interface BoxBorderModeAccessor {

	/**
	 * Retrieves the box border mode from the box border mode property.
	 * 
	 * @return The box border mode stored by the box border mode property.
	 */
	BoxBorderMode getBoxBorderMode();

	/**
	 * Provides a mutator for a box border mode property.
	 */
	public interface BoxBorderModeMutator {

		/**
		 * Sets the box border mode for the box border mode property.
		 * 
		 * @param aBoxBorderMode The box border mode to be stored by the box
		 *        border mode property.
		 */
		void setBoxBorderMode( BoxBorderMode aBoxBorderMode );
	}

	/**
	 * Provides a builder method for a box border mode property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface BoxBorderModeBuilder<B extends BoxBorderModeBuilder<B>> {

		/**
		 * Sets the box border mode for the box border mode property.
		 * 
		 * @param aBoxBorderMode The box border mode to be stored by the box
		 *        border mode property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBoxBorderMode( BoxBorderMode aBoxBorderMode );
	}

	/**
	 * Provides a box border mode property.
	 */
	public interface BoxBorderModeProperty extends BoxBorderModeAccessor, BoxBorderModeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link BoxBorderMode}
		 * (setter) as of {@link #setBoxBorderMode(BoxBorderMode)} and returns
		 * the very same value (getter).
		 * 
		 * @param aBoxBorderMode The {@link BoxBorderMode} to set (via
		 *        {@link #setBoxBorderMode(BoxBorderMode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default BoxBorderMode letBoxBorderMode( BoxBorderMode aBoxBorderMode ) {
			setBoxBorderMode( aBoxBorderMode );
			return aBoxBorderMode;
		}
	}
}
