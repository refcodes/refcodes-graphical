// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Enumeration providing definitions for common color depths used by various
 * raster image formats (pixmaps).
 */
public enum ColorDepth {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	MONOCHROME_1_BIT(1, 1, 1, 1, 1, 0, true),

	GRAYSCALE_8_BIT(8, 1, 8, 8, 8, 0, true),

	MSX_8_BIT(8, 1, 3, 3, 2, 0, false),

	HIGH_COLOR_16_BIT(16, 2, 5, 6, 5, 0, false),

	TRUE_COLOR_24_BIT(24, 3, 8, 8, 8, 0, false),

	AWT_COLOR_24_BIT(24, 3, 8, 8, 8, 0, false),

	TRUE_COLOR_32_BIT(32, 4, 8, 8, 8, 8, false);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _bits;
	private int _bytes;
	private int _redBits;
	private int _greenBits;
	private int _blueBits;
	private int _alphaBits;
	private int _maxValue;
	private int _maxRedValue;
	private int _maxGreenValue;
	private int _maxBlueValue;
	private int _maxAlphaValue;
	private boolean _isMonochrome;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private ColorDepth( int aBits, int aBytes, int aRedBits, int aGreenBits, int aBlueBits, int aAlphaBits, boolean isMonochrome ) {
		_bits = aBits;
		_bytes = aBytes;
		_redBits = aRedBits;
		_greenBits = aGreenBits;
		_blueBits = aBlueBits;
		_alphaBits = aAlphaBits;
		_maxValue = (int) ( Math.pow( 2, aBits ) - 1 );
		_maxRedValue = (int) ( Math.pow( 2, aRedBits ) - 1 );
		_maxGreenValue = (int) ( Math.pow( 2, aGreenBits ) - 1 );
		_maxBlueValue = (int) ( Math.pow( 2, aBlueBits ) - 1 );
		_maxAlphaValue = (int) ( Math.pow( 2, aAlphaBits ) - 1 );
		_isMonochrome = isMonochrome;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The overall number of bits per pixel.
	 * 
	 * @return the bits
	 */
	public int getBits() {
		return _bits;
	}

	/**
	 * Returns the number of bytes required to store the color value. For
	 * {@link ColorDepth} values with less than 8 bits per color, 1 will be
	 * returned, although a byte may contain more than one color. E.g.
	 * {@link #MONOCHROME_1_BIT} can store up to 8 color per byte!
	 * 
	 * @return The number of bytes for the color value.
	 */
	public int getBytes() {
		return _bytes;
	}

	/**
	 * The number of bits for the "red" portion.
	 * 
	 * @return the red
	 */
	public int getRedBits() {
		return _redBits;
	}

	/**
	 * The number of bits for the "green" portion.
	 * 
	 * @return the green
	 */
	public int getGreenBits() {
		return _greenBits;
	}

	/**
	 * The number of bits for the "blue" portion.
	 * 
	 * @return the blue
	 */
	public int getBlueBits() {
		return _blueBits;
	}

	/**
	 * The number of bits for the "alpha" portion.
	 * 
	 * @return the alpha
	 */
	public int getAlphaBits() {
		return _alphaBits;
	}

	/**
	 * The overall max value of per pixel.
	 * 
	 * @return the max value for
	 */
	public int getMaxValue() {
		return _maxValue;
	}

	/**
	 * The max value of for the "red" portion.
	 * 
	 * @return the max value for red
	 */
	public int getMaxRedValue() {
		return _maxRedValue;
	}

	/**
	 * The max value of for the "green" portion.
	 * 
	 * @return the max value for green
	 */
	public int getMaxGreenValue() {
		return _maxGreenValue;
	}

	/**
	 * The max value of for the "blue" portion.
	 * 
	 * @return the max value for blue
	 */
	public int getMaxBlueValue() {
		return _maxBlueValue;
	}

	/**
	 * The max value of for the "alpha" portion.
	 * 
	 * @return the max value for alpha
	 */
	public int getMaxAlphaValue() {
		return _maxAlphaValue;
	}

	/**
	 * Specifies whether the {@link ColorDepth} has neither RGB nor ALPHA
	 * portions. The color just specifies the brightness (in terms of greay
	 * scale or monochrome).
	 * 
	 * @return The in case the color dies not have any RGB nor ALPHA portions!
	 */
	public boolean isMonochrome() {
		return _isMonochrome;
	}

	/**
	 * Retrieves the value of the alpha portion for the provided color value
	 * (being of the current {@link ColorDepth} format).
	 * 
	 * @param aColorValue A color value of the current {@link ColorDepth} format
	 * 
	 * @return The according alpha portion.
	 */
	public int toAlphaValue( int aColorValue ) {
		if ( isMonochrome() ) {
			return getMaxAlphaValue();
		}
		final int shiftRight = getGreenBits() + getGreenBits() + getBlueBits();
		return ( ( aColorValue >> shiftRight ) & getMaxAlphaValue() ); //A->R->G->B

	}

	/**
	 * Retrieves the value of the red portion for the provided color value
	 * (being of the current {@link ColorDepth} format).
	 * 
	 * @param aColorValue A color value of the current {@link ColorDepth} format
	 * 
	 * @return The according red portion.
	 */
	public int toRedValue( int aColorValue ) {
		if ( isMonochrome() ) {
			return aColorValue;
		}
		final int shiftRight = getGreenBits() + getBlueBits();
		final int shifted = aColorValue >> shiftRight;
		return ( shifted & getMaxRedValue() ); //A->R->G->B
	}

	/**
	 * Retrieves the value of the green portion for the provided color value
	 * (being of the current {@link ColorDepth} format).
	 * 
	 * @param aColorValue A color value of the current {@link ColorDepth} format
	 * 
	 * @return The according green portion.
	 */
	public int toGreenValue( int aColorValue ) {
		if ( isMonochrome() ) {
			return aColorValue;
		}
		final int shiftRight = getBlueBits();
		return ( ( aColorValue >> shiftRight ) & getMaxGreenValue() ); //A->R->G->B

	}

	/**
	 * Retrieves the value of the blue portion for the provided color value
	 * (being of the current {@link ColorDepth} format).
	 * 
	 * @param aColorValue A color value of the current {@link ColorDepth} format
	 * 
	 * @return The according blue portion.
	 */
	public int toBlueValue( int aColorValue ) {
		if ( isMonochrome() ) {
			return aColorValue;
		}
		return ( aColorValue & getMaxBlueValue() ); //A->R->G->B
	}

	/**
	 * Converts a color from one color depth to another color depth.
	 * 
	 * @param aColor The color to be converted.
	 * 
	 * @param aColorDepth The {@link ColorDepth} of the proided color.
	 * 
	 * @return The converted color.
	 */
	public int toColor( int aColor, ColorDepth aColorDepth ) {
		if ( aColorDepth == this ) {
			return aColor;
		}
		final float theAlphaValue = aColorDepth.getMaxAlphaValue() != 0 ? ( (float) aColorDepth.toAlphaValue( aColor ) ) / ( (float) aColorDepth.getMaxAlphaValue() ) * ( (float) getMaxAlphaValue() ) : 0;
		final float theRedValue = aColorDepth.getMaxRedValue() != 0 ? ( (float) aColorDepth.toRedValue( aColor ) ) / ( (float) aColorDepth.getMaxRedValue() ) * ( (float) getMaxRedValue() ) : 0;
		final float theGreenValue = aColorDepth.getMaxGreenValue() != 0 ? ( (float) aColorDepth.toGreenValue( aColor ) ) / ( (float) aColorDepth.getMaxGreenValue() ) * ( (float) getMaxGreenValue() ) : 0;
		final float theBlueValue = aColorDepth.getMaxBlueValue() != 0 ? ( (float) aColorDepth.toBlueValue( aColor ) ) / ( (float) aColorDepth.getMaxBlueValue() ) * ( (float) getMaxBlueValue() ) : 0;
		int theColor = (int) theAlphaValue;
		theColor = theColor << getRedBits() | (int) theRedValue;
		theColor = theColor << getGreenBits() | (int) theGreenValue;
		theColor = theColor << getBlueBits() | (int) theBlueValue;
		return theColor;
	}
}