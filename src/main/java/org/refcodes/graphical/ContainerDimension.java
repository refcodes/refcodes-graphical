// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a {@link ContainerDimension} property.
 */
public interface ContainerDimension extends ContainerHeightAccessor, ContainerWidthAccessor {

	/**
	 * The Interface ContainerDimensionMutator.
	 */
	public interface ContainerDimensionMutator extends ContainerHeightMutator, ContainerWidthMutator {

		/**
		 * Sets the container dimension.
		 *
		 * @param aContainerWidth the container width
		 * @param aContainerHeight the container height
		 */
		void setContainerDimension( int aContainerWidth, int aContainerHeight );

		/**
		 * Sets the container dimension.
		 *
		 * @param aContainerDimension the new container dimension
		 */
		void setContainerDimension( ContainerDimension aContainerDimension );

		/**
		 * Sets the container dimension.
		 *
		 * @param aDimension the new container dimension
		 */
		void setContainerDimension( Dimension aDimension );
	}

	/**
	 * The Interface ContainerDimensionBuilder.
	 *
	 * @param <B> the generic type
	 */
	public interface ContainerDimensionBuilder<B extends ContainerDimensionBuilder<B>> extends ContainerWidthBuilder<B>, ContainerHeightBuilder<B> {

		/**
		 * With container dimension.
		 *
		 * @param aContainerWidth the container width
		 * @param aContainerHeight the container height
		 * 
		 * @return the b
		 */
		B withContainerDimension( int aContainerWidth, int aContainerHeight );

		/**
		 * With container dimension.
		 *
		 * @param aContainerDimension the container dimension
		 * 
		 * @return the b
		 */
		B withContainerDimension( ContainerDimension aContainerDimension );

		/**
		 * With container dimension.
		 *
		 * @param aDimension the dimension
		 * 
		 * @return the b
		 */
		B withContainerDimension( Dimension aDimension );
	}

	/**
	 * The Interface ContainerDimensionProperty.
	 */
	public interface ContainerDimensionProperty extends ContainerDimension, ContainerDimensionMutator, ContainerWidthProperty, ContainerHeightProperty {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link ContainerDimension} (setter) as of
		 * {@link #setContainerDimension(ContainerDimension)} and returns the
		 * very same value (getter).
		 * 
		 * @param aContainerDimension The {@link ContainerDimension} to set (via
		 *        {@link #setContainerDimension(ContainerDimension)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ContainerDimension letContainerDimension( ContainerDimension aContainerDimension ) {
			setContainerDimension( aContainerDimension );
			return aContainerDimension;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setContainerDimension(Dimension)} and returns
		 * the very same value (getter).
		 * 
		 * @param aDimension The {@link Dimension} to set (via
		 *        {@link #setContainerDimension(Dimension)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Dimension letContainerDimension( Dimension aDimension ) {
			setContainerDimension( aDimension );
			return aDimension;
		}

		/**
		 * This method stores and passes through the given arguments, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setContainerDimension(int, int)} and returns
		 * the very same values encapsulated as {@link ContainerDimension}
		 * instance.
		 * 
		 * @param aContainerWidth The width {@link Dimension} to set (via
		 *        {@link #setContainerWidth(int)}).
		 * @param aContainerHeight The height {@link Dimension} to set (via
		 *        {@link #setContainerHeight(int)}).
		 * 
		 * @return Returns the values passed encapsulated in a
		 *         {@link ContainerDimension} object for it to be used in
		 *         conclusive processing steps.
		 */
		default ContainerDimension letContainerDimension( int aContainerWidth, int aContainerHeight ) {
			setContainerDimension( aContainerWidth, aContainerHeight );
			return new ContainerDimension() {
				@Override
				public int getContainerWidth() {
					return aContainerWidth;
				}

				@Override
				public int getContainerHeight() {
					return aContainerHeight;
				}
			};
		}
	}

	/**
	 * Equals.
	 *
	 * @param aContainerDimensionA the container dimension A
	 * @param aContainerDimensionB the container dimension B
	 * 
	 * @return true, if successful
	 */
	static boolean equals( ContainerDimension aContainerDimensionA, ContainerDimension aContainerDimensionB ) {
		return aContainerDimensionA.getContainerWidth() == aContainerDimensionB.getContainerWidth() && aContainerDimensionA.getContainerHeight() == aContainerDimensionB.getContainerHeight();
	}
}
