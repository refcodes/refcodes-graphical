/*
 * 
 */
package org.refcodes.graphical;

/**
 * The Interface ContainerMetrics.
 */
public interface ContainerMetrics {

	/**
	 * To total width.
	 *
	 * @return the int
	 */
	int toTotalWidth();

	/**
	 * To total height.
	 *
	 * @return the int
	 */
	int toTotalHeight();

}