// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a container width property.
 */
public interface ContainerWidthAccessor {

	/**
	 * Retrieves the container width from the container width property.
	 * 
	 * @return The container width stored by the container width property.
	 */
	int getContainerWidth();

	/**
	 * Provides a mutator for a container width property.
	 */
	public interface ContainerWidthMutator {

		/**
		 * Sets the container width for the container width property.
		 * 
		 * @param aWidth The container width to be stored by the container width
		 *        property.
		 */
		void setContainerWidth( int aWidth );
	}

	/**
	 * Provides a builder method for a container width property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ContainerWidthBuilder<B extends ContainerWidthBuilder<B>> {

		/**
		 * Sets the container width for the container width property.
		 * 
		 * @param aWidth The container width to be stored by the container width
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withContainerWidth( int aWidth );
	}

	/**
	 * Provides a container width property.
	 */
	public interface ContainerWidthProperty extends ContainerWidthAccessor, ContainerWidthMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given container width (setter)
		 * as of {@link #setContainerWidth(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aContainerWidth The container width to set (via
		 *        {@link #setContainerWidth(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letContainerWidth( int aContainerWidth ) {
			setContainerWidth( aContainerWidth );
			return aContainerWidth;
		}
	}
}
