// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a content property.
 *
 * @param <T> The content's type to be accessed.
 */
public interface ContentAccessor<T> {

	/**
	 * Retrieves the content from the content property.
	 * 
	 * @return The content stored by the content property.
	 */
	T getContent();

	/**
	 * Provides a mutator for a content property.
	 * 
	 * @param <T> The content's type to be accessed.
	 */
	public interface ContentMutator<T> {

		/**
		 * Sets the content for the content property.
		 * 
		 * @param aContent The content to be stored by the content property.
		 */
		void setContent( T aContent );
	}

	/**
	 * Provides a builder method for a content property returning the builder
	 * for applying multiple build operations.
	 *
	 * @param <T> the generic type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ContentBuilder<T, B extends ContentBuilder<T, B>> {

		/**
		 * Sets the content for the content property.
		 * 
		 * @param aContent The content to be stored by the content property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withContent( T aContent );
	}

	/**
	 * Provides a content property.
	 * 
	 * @param <T> The content's type to be accessed.
	 */
	public interface ContentProperty<T> extends ContentAccessor<T>, ContentMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given content (setter) as of
		 * {@link #setContent(Object)} and returns the very same value (getter).
		 * 
		 * @param aContent The content to set (via {@link #setContent(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letContent( T aContent ) {
			setContent( aContent );
			return aContent;
		}
	}
}
