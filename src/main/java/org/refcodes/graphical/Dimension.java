// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a dimension property (width and height).
 */
public interface Dimension extends WidthAccessor, HeightAccessor {

	/**
	 * Provides an accessor for a dimension property (width and height).
	 */
	public interface DimensionAccessor extends Dimension {}

	/**
	 * Provides a mutator for a dimension property (width and height).
	 */
	public interface DimensionMutator extends WidthMutator, HeightMutator {

		/**
		 * Sets the dimension.
		 *
		 * @param aWidth the width
		 * @param aHeight the height
		 */
		void setDimension( int aWidth, int aHeight );

		/**
		 * Sets the dimension.
		 *
		 * @param aDimension the new dimension
		 */
		void setDimension( Dimension aDimension );
	}

	/**
	 * Provides a builder for a dimension property (width and height).
	 *
	 * @param <B> the generic type
	 */
	public interface DimensionBuilder<B extends DimensionBuilder<B>> extends WidthBuilder<B>, HeightBuilder<B> {

		/**
		 * With dimension.
		 *
		 * @param aWidth the width
		 * @param aHeight the height
		 * 
		 * @return the b
		 */
		B withDimension( int aWidth, int aHeight );

		/**
		 * With dimension.
		 *
		 * @param aDimension the dimension
		 * 
		 * @return the b
		 */
		B withDimension( Dimension aDimension );
	}

	/**
	 * Provides a property (getter / setter) for a dimension property (width and
	 * height).
	 */
	public interface DimensionProperty extends DimensionAccessor, DimensionMutator, WidthProperty, HeightProperty {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setDimension(Dimension)} and returns the very
		 * same value (getter).
		 * 
		 * @param aDimension The {@link Dimension} to set (via
		 *        {@link #setDimension(Dimension)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Dimension letDimension( Dimension aDimension ) {
			setDimension( aDimension );
			return aDimension;
		}

		/**
		 * This method stores and passes through the given arguments, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setDimension(int, int)} and returns the very
		 * same value encapsulated as {@link Dimension} instance.
		 * 
		 * @param aWidth The width {@link Dimension} to set (via
		 *        {@link #setWidth(int)}).
		 * @param aHeight The height {@link Dimension} to set (via
		 *        {@link #setHeight(int)}).
		 * 
		 * @return Returns the values passed encapsulated in a {@link Dimension}
		 *         object for it to be used in conclusive processing steps.
		 */
		default Dimension letDimension( int aWidth, int aHeight ) {
			setDimension( aWidth, aHeight );
			return new DimensionImpl( aWidth, aHeight );
		}
	}

	/**
	 * Equals.
	 *
	 * @param aDimensionA the dimension A
	 * @param aDimensionB the dimension B
	 * 
	 * @return true, if successful
	 */
	static boolean equals( Dimension aDimensionA, Dimension aDimensionB ) {
		return aDimensionA.getWidth() == aDimensionB.getWidth() && aDimensionA.getHeight() == aDimensionB.getHeight();
	}
}
