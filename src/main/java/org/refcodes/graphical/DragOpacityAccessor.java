// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a drag opacity property.
 */
public interface DragOpacityAccessor {

	/**
	 * Retrieves the drag opacity from the drag opacity property.
	 * 
	 * @return The drag opacity stored by the drag opacity property.
	 */
	double getDragOpacity();

	/**
	 * Provides a mutator for a drag opacity property.
	 */
	public interface DragOpacityMutator {

		/**
		 * Sets the drag opacity for the drag opacity property.
		 * 
		 * @param aOpacity The drag opacity to be stored by the drag opacity
		 *        property.
		 */
		void setDragOpacity( double aOpacity );

	}

	/**
	 * Provides a builder method for a drag opacity property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface DragOpacityBuilder<B extends DragOpacityBuilder<B>> {

		/**
		 * Sets the drag opacity for the drag opacity property.
		 * 
		 * @param aOpacity The drag opacity to be stored by the drag opacity
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withDragOpacity( double aOpacity );
	}

	/**
	 * Provides a drag opacity property.
	 */
	public interface DragOpacityProperty extends DragOpacityAccessor, DragOpacityMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given double (setter) as of
		 * {@link #setDragOpacity(double)} and returns the very same value
		 * (getter).
		 * 
		 * @param aDragOpacity The double to set (via
		 *        {@link #setDragOpacity(double)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default double letDragOpacity( double aDragOpacity ) {
			setDragOpacity( aDragOpacity );
			return aDragOpacity;
		}
	}
}
