// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a draggable property.
 */
public interface DraggableAccessor {

	/**
	 * Retrieves the draggability from the draggable property.
	 * 
	 * @return The draggability stored by the draggable property.
	 */
	boolean isDraggable();

	/**
	 * Provides a mutator for a draggable property.
	 */
	public interface DraggableMutator {

		/**
		 * Sets the draggability for the draggable property.
		 * 
		 * @param isDraggable The draggability to be stored by the draggability
		 *        property.
		 */
		void setDraggable( boolean isDraggable );

		/**
		 * Same as setting {@link #setDraggable(boolean)} to true.
		 */
		void draggable();

		/**
		 * Same as setting {@link #setDraggable(boolean)} to false.
		 */
		void stationary();
	}

	/**
	 * Provides a builder method for a draggable property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface DraggableBuilder<B extends DraggableBuilder<B>> {

		/**
		 * Sets the draggability for the draggable property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withDraggable();

		/**
		 * Sets the draggability for the draggable property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withStationary();

		/**
		 * Sets the draggability for the draggable property.
		 * 
		 * @param isDraggable The draggability to be stored by the draggability
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withDraggable( boolean isDraggable );
	}

	/**
	 * Provides a draggable property.
	 */
	public interface DraggableProperty extends DraggableAccessor, DraggableMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean (setter) as of
		 * {@link #setDraggable(boolean)} and returns the very same value
		 * (getter).
		 * 
		 * @param isDraggable The boolean to set (via
		 *        {@link #setDraggable(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letDraggable( boolean isDraggable ) {
			setDraggable( isDraggable );
			return isDraggable;
		}
	}
}
