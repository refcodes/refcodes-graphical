// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a {@link FieldDimension} property.
 */
public interface FieldDimension extends FieldHeightAccessor, FieldWidthAccessor, FieldGapAccessor {

	/**
	 * Provides an accessor for a {@link FieldDimension} property.
	 */
	public interface FieldDimensionAccessor extends FieldDimension {}

	/**
	 * The Interface FieldDimensionMutator.
	 */
	public interface FieldDimensionMutator extends FieldHeightMutator, FieldWidthMutator, FieldGapMutator {

		/**
		 * Sets the field dimension.
		 *
		 * @param aFieldWidth the field width
		 * @param aFieldHeight the field height
		 */
		void setFieldDimension( int aFieldWidth, int aFieldHeight );

		/**
		 * Sets the field dimension.
		 *
		 * @param aFieldWidth the field width
		 * @param aFieldHeight the field height
		 * @param aGap the gap
		 */
		void setFieldDimension( int aFieldWidth, int aFieldHeight, int aGap );

		/**
		 * Sets the field dimension.
		 *
		 * @param aField the new field dimension
		 */
		void setFieldDimension( FieldDimension aField );

		/**
		 * Sets the field dimension.
		 *
		 * @param aDimension the new field dimension
		 */
		void setFieldDimension( Dimension aDimension );
	}

	/**
	 * The Interface FieldDimensionBuilder.
	 *
	 * @param <B> the generic type
	 */
	public interface FieldDimensionBuilder<B extends FieldDimensionBuilder<B>> extends FieldWidthBuilder<B>, FieldHeightBuilder<B>, FieldGapBuilder<B> {

		/**
		 * With field dimension.
		 *
		 * @param aFieldWidth the field width
		 * @param aFieldHeight the field height
		 * 
		 * @return the b
		 */
		B withFieldDimension( int aFieldWidth, int aFieldHeight );

		/**
		 * With field dimension.
		 *
		 * @param aFieldWidth the field width
		 * @param aFieldHeight the field height
		 * @param aGap the gap
		 * 
		 * @return the b
		 */
		B withFieldDimension( int aFieldWidth, int aFieldHeight, int aGap );

		/**
		 * With field dimension.
		 *
		 * @param aField the field
		 * 
		 * @return the b
		 */
		B withFieldDimension( FieldDimension aField );

		/**
		 * With field dimension.
		 *
		 * @param aDimension the dimension
		 * 
		 * @return the b
		 */
		B withFieldDimension( Dimension aDimension );
	}

	/**
	 * The Interface FieldDimensionProperty.
	 */
	public interface FieldDimensionProperty extends FieldDimensionAccessor, FieldDimensionMutator, FieldWidthProperty, FieldHeightProperty, FieldGapProperty {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link FieldDimension}
		 * (setter) as of {@link #setFieldDimension(FieldDimension)} and returns
		 * the very same value (getter).
		 * 
		 * @param aFieldDimension The {@link FieldDimension} to set (via
		 *        {@link #setFieldDimension(FieldDimension)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default FieldDimension letFieldDimension( FieldDimension aFieldDimension ) {
			setFieldDimension( aFieldDimension );
			return aFieldDimension;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setFieldDimension(Dimension)} and returns the
		 * very same value (getter).
		 * 
		 * @param aDimension The {@link Dimension} to set (via
		 *        {@link #setFieldDimension(Dimension)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Dimension letFieldDimension( Dimension aDimension ) {
			setFieldDimension( aDimension );
			return aDimension;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setFieldDimension(int, int)} and returns the
		 * very same value encapsulated as {@link Dimension} instance.
		 * 
		 * @param aFieldWidth The width {@link Dimension} to set (via
		 *        {@link #setFieldWidth(int)}).
		 * @param aFieldHeight The height {@link Dimension} to set (via
		 *        {@link #setFieldHeight(int)}).
		 * 
		 * @return Returns the values passed encapsulated in a {@link Dimension}
		 *         object for it to be used in conclusive processing steps.
		 */
		default Dimension letFieldDimension( int aFieldWidth, int aFieldHeight ) {
			setFieldDimension( aFieldWidth, aFieldHeight );
			return new Dimension() {
				@Override
				public int getWidth() {
					return aFieldWidth;
				}

				@Override
				public int getHeight() {
					return aFieldHeight;
				}
			};
		}

		/**
		 * This method stores and passes through the given arguments, which is
		 * very useful for builder APIs: Sets the given {@link Dimension} as
		 * well as the gap (setter) as of
		 * {@link #setFieldDimension(int, int, int)} and returns the very same
		 * values encapsulated as {@link FieldDimension} instance.
		 * 
		 * @param aFieldWidth The width {@link Dimension} to set (via
		 *        {@link #setFieldWidth(int)}).
		 * @param aFieldHeight The height {@link Dimension} to set (via
		 *        {@link #setFieldHeight(int)}).
		 * @param aGap The according gap.
		 * 
		 * @return Returns the values passed encapsulated in a
		 *         {@link FieldDimension} object for it to be used in conclusive
		 *         processing steps.
		 */
		default FieldDimension letFieldDimension( int aFieldWidth, int aFieldHeight, int aGap ) {
			setFieldDimension( aFieldWidth, aFieldHeight, aGap );
			return new FieldDimension() {
				@Override
				public int getFieldWidth() {
					return aFieldWidth;
				}

				@Override
				public int getFieldHeight() {
					return aFieldHeight;
				}

				@Override
				public int getFieldGap() {
					return aGap;
				}
			};
		}
	}

	/**
	 * Equals.
	 *
	 * @param aFieldDimensionA the field dimension A
	 * @param aFieldDimensionB the field dimension B
	 * 
	 * @return true, if successful
	 */
	static boolean equals( FieldDimension aFieldDimensionA, FieldDimension aFieldDimensionB ) {
		return aFieldDimensionA.getFieldWidth() == aFieldDimensionB.getFieldWidth() && aFieldDimensionA.getFieldHeight() == aFieldDimensionB.getFieldHeight();
	}
}
