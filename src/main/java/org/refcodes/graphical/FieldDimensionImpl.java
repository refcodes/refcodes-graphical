// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * The Class FieldDimensionImpl.
 *
 * @author steiner
 */
public class FieldDimensionImpl implements FieldDimension {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected int _fieldWidth;

	protected int _fieldHeight;

	protected int _fieldGap;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new field dimension impl.
	 */
	protected FieldDimensionImpl() {}

	/**
	 * Instantiates a new field dimension impl.
	 *
	 * @param aWidth the width
	 * @param aHeight the height
	 * @param aFieldGap the field gap
	 */
	public FieldDimensionImpl( int aWidth, int aHeight, int aFieldGap ) {
		_fieldWidth = aWidth;
		_fieldHeight = aHeight;
		_fieldGap = aFieldGap;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFieldWidth() {
		return _fieldWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFieldHeight() {
		return _fieldHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFieldGap() {
		return _fieldGap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + _fieldWidth + ":" + _fieldHeight + ", " + _fieldGap + ")@" + hashCode();
	}
}