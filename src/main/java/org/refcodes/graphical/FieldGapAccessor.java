// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a field gap property.
 */
public interface FieldGapAccessor {

	/**
	 * Retrieves the field gap from the field gap property.
	 * 
	 * @return The field gap stored by the field gap property.
	 */
	int getFieldGap();

	/**
	 * Provides a mutator for a field gap property.
	 */
	public interface FieldGapMutator {

		/**
		 * Sets the field gap for the field gap property.
		 * 
		 * @param aFieldGap The field gap to be stored by the field gap
		 *        property.
		 */
		void setFieldGap( int aFieldGap );
	}

	/**
	 * Provides a builder method for a field gap property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FieldGapBuilder<B extends FieldGapBuilder<B>> {

		/**
		 * Sets the field gap for the field gap property.
		 * 
		 * @param aFieldGap The field gap to be stored by the field gap
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFieldGap( int aFieldGap );
	}

	/**
	 * Provides a field gap property.
	 */
	public interface FieldGapProperty extends FieldGapAccessor, FieldGapMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setFieldGap(int)} and returns the very same value (getter).
		 * 
		 * @param aFieldGap The integer to set (via {@link #setFieldGap(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letFieldGap( int aFieldGap ) {
			setFieldGap( aFieldGap );
			return aFieldGap;
		}
	}
}
