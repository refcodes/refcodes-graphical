// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a field height property.
 */
public interface FieldHeightAccessor {

	/**
	 * Retrieves the field height from the field height property.
	 * 
	 * @return The field height stored by the field height property.
	 */
	int getFieldHeight();

	/**
	 * Provides a mutator for a field height property.
	 */
	public interface FieldHeightMutator {

		/**
		 * Sets the field height for the field height property.
		 * 
		 * @param aHeight The field height to be stored by the field height
		 *        property.
		 */
		void setFieldHeight( int aHeight );
	}

	/**
	 * Provides a builder method for a field height property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FieldHeightBuilder<B extends FieldHeightBuilder<B>> {

		/**
		 * Sets the field height for the field height property.
		 * 
		 * @param aHeight The field height to be stored by the field height
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFieldHeight( int aHeight );
	}

	/**
	 * Provides a field height property.
	 */
	public interface FieldHeightProperty extends FieldHeightAccessor, FieldHeightMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setFieldHeight(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aFieldHeight The integer to set (via
		 *        {@link #setFieldHeight(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letFieldHeight( int aFieldHeight ) {
			setFieldHeight( aFieldHeight );
			return aFieldHeight;
		}
	}
}
