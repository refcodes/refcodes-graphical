// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a field width property.
 */
public interface FieldWidthAccessor {

	/**
	 * Retrieves the field width from the field width property.
	 * 
	 * @return The field width stored by the field width property.
	 */
	int getFieldWidth();

	/**
	 * Provides a mutator for a field width property.
	 */
	public interface FieldWidthMutator {

		/**
		 * Sets the field width for the field width property.
		 * 
		 * @param aWidth The field width to be stored by the field width
		 *        property.
		 */
		void setFieldWidth( int aWidth );
	}

	/**
	 * Provides a builder method for a field width property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FieldWidthBuilder<B extends FieldWidthBuilder<B>> {

		/**
		 * Sets the field width for the field width property.
		 * 
		 * @param aWidth The field width to be stored by the field width
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFieldWidth( int aWidth );
	}

	/**
	 * Provides a field width property.
	 */
	public interface FieldWidthProperty extends FieldWidthAccessor, FieldWidthMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setFieldWidth(int)} and returns the very same value (getter).
		 * 
		 * @param aFieldWidth The integer to set (via
		 *        {@link #setFieldWidth(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letFieldWidth( int aFieldWidth ) {
			setFieldWidth( aFieldWidth );
			return aFieldWidth;
		}
	}
}
