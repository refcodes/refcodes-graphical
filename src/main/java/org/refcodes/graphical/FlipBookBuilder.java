/*
 * 
 */
package org.refcodes.graphical;

import org.refcodes.component.Ceasable.UncheckedCeasable;
import org.refcodes.component.LifecycleComponent.UncheckedLifecycleComponent;

/**
 * A {@link FlipBookBuilder} defines a sequence of images (animation) to be
 * flipped through (as of an old fashioned flip-book). The
 * {@link FlipBookBuilder} supports different phases initiated by the according
 * life-cycle phases: INITIALIZE THE SEQUENCE: {@link #initialize()}: Reset the
 * {@link FlipBookBuilder} to start the animation over again with the start-up
 * sequence. The start-up sequence is displayed just once to commence the
 * main-loop sequence. Note: The {@link FlipBookBuilder} animation is not yet
 * started, just prepared to begin with the start-up sequence! START THE
 * SEQUENCE: {@link #start()}: Starts the {@link FlipBookBuilder} animation. In
 * case there is a start-up sequence, that sequence is flipped through till the
 * end followed by the main-loop sequence which is being repeated until
 * instructed differently (via {@link #stop()}, {@link #pause()} or
 * {@link #destroy()}). PAUSE THE SEQUENCE: {@link #pause()}: Pauses the
 * animation just where it is currently (to be resumed later with
 * {@link #resume()}). RESUME THE SEQUENCE: {@link #resume()}: Resumes a paused
 * ({@link #pause()}) the animation to continue just where it has been paused.
 * STOP THE SEQUENCE: {@link #stop()}: Stops the animation sequence at the
 * beginning of the main-loop. The animation continues till the beginning main
 * loop reached its sequence's end, then it stops. CEASE THE SEQUENCE:
 * {@link #cease()}: In case you want to fade out your sequence or let it end
 * with an explosion, you may invoke the {@link #cease()} method. The animation
 * should be initialized ({@link #initialize()}) in order to begin with the
 * start-up sequence again.
 * 
 * @param <IMG> The image type to be used for the {@link FlipBookBuilder}.
 */
public interface FlipBookBuilder<IMG> extends UncheckedLifecycleComponent, UncheckedCeasable {

	/**
	 * Adds an image to the start-up sequence.
	 * 
	 * @param aImage The image to be added to the end of the start-up sequence.
	 */
	void addStartUpImage( IMG aImage );

	/**
	 * Adds an image to the start-up sequence.
	 * 
	 * @param aImage The image to be added to the end of the start-up sequence.
	 * 
	 * @return The {@link FlipBookBuilder} as of the Builder-Pattern.
	 */
	default FlipBookBuilder<IMG> withAddStartUpImage( IMG aImage ) {
		addStartUpImage( aImage );
		return this;
	}

	/**
	 * Adds a sequence of images to the start-up sequence.
	 * 
	 * @param aImages The images to be added to the end of the start-up
	 *        sequence.
	 */
	@SuppressWarnings("unchecked")
	default void addStartUpSequence( IMG... aImages ) {
		for ( IMG eImage : aImages ) {
			addStartUpImage( eImage );
		}
	}

	/**
	 * Adds a sequence of images to the start-up sequence.
	 * 
	 * @param aImages The images to be added to the end of the start-up
	 *        sequence.
	 * 
	 * @return The {@link FlipBookBuilder} as of the Builder-Pattern.
	 */
	@SuppressWarnings("unchecked")
	default FlipBookBuilder<IMG> withAddStartUpSequence( IMG... aImages ) {
		addStartUpSequence( aImages );
		return this;
	}

	/**
	 * Adds an image to the main-loop sequence.
	 * 
	 * @param aImage The image to be added to the end of the main-loop sequence.
	 */
	void addMainLoopImage( IMG aImage );

	/**
	 * Adds an image to the main-loop sequence.
	 * 
	 * @param aImage The image to be added to the end of the main-loop sequence.
	 * 
	 * @return The {@link FlipBookBuilder} as of the Builder-Pattern.
	 */
	default FlipBookBuilder<IMG> withAddMainLoopImage( IMG aImage ) {
		addMainLoopImage( aImage );
		return this;
	}

	/**
	 * Adds a sequence of images to the main-loop sequence.
	 * 
	 * @param aImages The images to be added to the end of the main-loop
	 *        sequence.
	 */
	@SuppressWarnings("unchecked")
	default void addMainLoopSequence( IMG... aImages ) {
		for ( IMG eImage : aImages ) {
			addMainLoopImage( eImage );
		}
	}

	/**
	 * Adds a sequence of images to the main-loop sequence.
	 * 
	 * @param aImages The images to be added to the end of the main-loop
	 *        sequence.
	 * 
	 * @return The {@link FlipBookBuilder} as of the Builder-Pattern.
	 */
	@SuppressWarnings("unchecked")
	default FlipBookBuilder<IMG> withAddMainLoopSequence( IMG... aImages ) {
		addMainLoopSequence( aImages );
		return this;
	}

	/**
	 * Adds an image to the cease sequence.
	 * 
	 * @param aImage The image to be added to the end of the cease sequence.
	 */
	void addCeaseImage( IMG aImage );

	/**
	 * Adds an image to the cease sequence.
	 * 
	 * @param aImage The image to be added to the end of the cease sequence.
	 * 
	 * @return The {@link FlipBookBuilder} as of the Builder-Pattern.
	 */
	default FlipBookBuilder<IMG> withAddCeaseImage( IMG aImage ) {
		addCeaseImage( aImage );
		return this;
	}

	/**
	 * Adds a sequence of images to the cease sequence.
	 * 
	 * @param aImages The images to be added to the end of the cease sequence.
	 */
	@SuppressWarnings("unchecked")
	default void addCeaseSequence( IMG... aImages ) {
		for ( IMG eImage : aImages ) {
			addCeaseImage( eImage );
		}
	}

	/**
	 * Adds a sequence of images to the cease sequence.
	 * 
	 * @param aImages The images to be added to the end of the cease sequence.
	 * 
	 * @return The {@link FlipBookBuilder} as of the Builder-Pattern.
	 */
	@SuppressWarnings("unchecked")
	default FlipBookBuilder<IMG> withAddCeaseSequence( IMG... aImages ) {
		addCeaseSequence( aImages );
		return this;
	}

	/**
	 * Sets the duration of a single image to be displayed until the next image
	 * is shown.
	 * 
	 * @param aDurationInMs The duration in milliseconds.
	 */
	void setImageDurationInMs( int aDurationInMs );

	/**
	 * Gets the duration of a single image to be displayed until the next image
	 * is shown.
	 * 
	 * @return The duration in milliseconds.
	 */
	int getImageDurationInMs();

	/**
	 * Sets the duration of a single image to be displayed until the next image
	 * is shown.
	 * 
	 * @param aDurationInMs The duration in milliseconds.
	 * 
	 * @return The {@link FlipBookBuilder} as of the Builder-Pattern.
	 */
	default FlipBookBuilder<IMG> withImageDurationInMs( int aDurationInMs ) {
		setImageDurationInMs( aDurationInMs );
		return this;
	}

	/**
	 * Sets the start-up delay of the animation before it is started.
	 * 
	 * @param aStartUpDelayInMs The start-up delay in milliseconds.
	 */
	void setStartUpDelayInMs( int aStartUpDelayInMs );

	/**
	 * Gets the start-up delay of the animation before it is started.
	 * 
	 * @return The start-up delay in milliseconds.
	 */
	int getStartUpDelayInMs();

	/**
	 * Sets the start-up delay of the animation before it is started.
	 * 
	 * @param aStartUpDelayInMs The start-up delay in milliseconds.
	 * 
	 * @return The {@link FlipBookBuilder} as of the Builder-Pattern.
	 */
	default FlipBookBuilder<IMG> withStartUpDelayInMs( int aStartUpDelayInMs ) {
		setStartUpDelayInMs( aStartUpDelayInMs );
		return this;
	}
}
