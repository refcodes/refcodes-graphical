// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a {@link GridDimension} property.
 */
public interface GridDimension extends GridHeightAccessor, GridWidthAccessor {

	/**
	 * Provides an accessor for a {@link GridDimension} property.
	 */
	public interface GridDimensionAccessor extends GridDimension {}

	/**
	 * Tests whether the given position is inside of the bounds of the grid. The
	 * x and y coordinates must be greater than or equals to 0 and smaller than
	 * the grid width (x) and grid height (y).
	 * 
	 * @param aPosition The position for which to determine if it is in bounds.
	 * 
	 * @return True in case the given position is in bounds.
	 */
	default boolean isInBounds( Position aPosition ) {
		return isInBounds( aPosition.getPositionX(), aPosition.getPositionY() );
	}

	/**
	 * Tests whether the given position is inside of the bounds of the grid. The
	 * x- and y-coordinates must be greater or equals to 0 and smaller than the
	 * grid width (x) and grid height (y).
	 * 
	 * @param aPositionX The x-position for which to determine if it is in
	 *        bounds regarding the grid width.
	 * @param aPositionY The y-position for which to determine if it is in
	 *        bounds regarding the grid height.
	 * 
	 * @return True in case the given position is in bounds.
	 */
	default boolean isInBounds( int aPositionX, int aPositionY ) {
		return aPositionX < getGridWidth() && aPositionY < getGridHeight() && aPositionX >= 0 && aPositionY >= 0;
	}

	/**
	 * Tests whether the given x-position is inside of the bounds of the grid's
	 * width. The x-coordinate must be greater than or equals to 0 and smaller
	 * than the grid width.
	 * 
	 * @param aPositionX The x-position for which to determine if it is in
	 *        bounds regarding the grid width.
	 * 
	 * @return True in case the given x-position is in bounds.
	 */
	default boolean isInWidth( int aPositionX ) {
		return aPositionX < getGridWidth() && aPositionX >= 0;
	}

	/**
	 * Tests whether the given y-position is inside of the bounds of the grid's
	 * height. The y-coordinate must be greater than or equals to 0 and smaller
	 * than the grid height.
	 * 
	 * @param aPositionY The y-position for which to determine if it is in
	 *        bounds regarding the grid height.
	 * 
	 * @return True in case the given y-position is in bounds.
	 */
	default boolean isInHeight( int aPositionY ) {
		return aPositionY < getGridHeight() && aPositionY >= 0;
	}

	/**
	 * The Interface GridDimensionMutator.
	 */
	public interface GridDimensionMutator extends GridHeightMutator, GridWidthMutator {

		/**
		 * Sets the grid dimension.
		 *
		 * @param aWidth the width
		 * @param aHeight the height
		 */
		void setGridDimension( int aWidth, int aHeight );

		/**
		 * Sets the grid dimension.
		 *
		 * @param aDimension the new grid dimension
		 */
		void setGridDimension( GridDimension aDimension );

		/**
		 * Sets the grid dimension.
		 *
		 * @param aDimension the new grid dimension
		 */
		void setGridDimension( Dimension aDimension );
	}

	/**
	 * The Interface GridDimensionBuilder.
	 *
	 * @param <B> the generic type
	 */
	public interface GridDimensionBuilder<B extends GridDimensionBuilder<B>> extends GridWidthBuilder<B>, GridHeightBuilder<B> {

		/**
		 * With grid dimension.
		 *
		 * @param aGridWidth the grid width
		 * @param aGridHeight the grid height
		 * 
		 * @return the b
		 */
		B withGridDimension( int aGridWidth, int aGridHeight );

		/**
		 * With grid dimension.
		 *
		 * @param aDimension the dimension
		 * 
		 * @return the b
		 */
		B withGridDimension( GridDimension aDimension );

		/**
		 * With grid dimension.
		 *
		 * @param aDimension the dimension
		 * 
		 * @return the b
		 */
		B withGridDimension( Dimension aDimension );
	}

	/**
	 * The Interface GridDimensionProperty.
	 */
	public interface GridDimensionProperty extends GridDimensionAccessor, GridDimensionMutator, GridWidthProperty, GridHeightProperty {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link GridDimension}
		 * (setter) as of {@link #setGridDimension(GridDimension)} and returns
		 * the very same value (getter).
		 * 
		 * @param aGridDimension The {@link GridDimension} to set (via
		 *        {@link #setGridDimension(GridDimension)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default GridDimension letGridDimension( GridDimension aGridDimension ) {
			setGridDimension( aGridDimension );
			return aGridDimension;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setGridDimension(Dimension)} and returns the
		 * very same value (getter).
		 * 
		 * @param aDimension The {@link Dimension} to set (via
		 *        {@link #setGridDimension(Dimension)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Dimension letGridDimension( Dimension aDimension ) {
			setGridDimension( aDimension );
			return aDimension;
		}

		/**
		 * This method stores and passes through the given arguments, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setGridDimension(int, int)} and returns the
		 * very same values encapsulated as {@link GridDimension} instance.
		 * 
		 * @param aGridWidth The width {@link Dimension} to set (via
		 *        {@link #setGridWidth(int)}).
		 * @param aGridHeight The height {@link Dimension} to set (via
		 *        {@link #setGridHeight(int)}).
		 * 
		 * @return Returns the values passed encapsulated in a
		 *         {@link GridDimension} object for it to be used in conclusive
		 *         processing steps.
		 */
		default GridDimension letGridDimension( int aGridWidth, int aGridHeight ) {
			setGridDimension( aGridWidth, aGridHeight );
			return new GridDimension() {
				@Override
				public int getGridWidth() {
					return aGridWidth;
				}

				@Override
				public int getGridHeight() {
					return aGridHeight;
				}
			};
		}
	}

	/**
	 * Equals.
	 *
	 * @param aGridDimensionA the grid dimension A
	 * @param aGridDimensionB the grid dimension B
	 * 
	 * @return true, if successful
	 */
	static boolean equals( GridDimension aGridDimensionA, GridDimension aGridDimensionB ) {
		return aGridDimensionA.getGridWidth() == aGridDimensionB.getGridWidth() && aGridDimensionA.getGridHeight() == aGridDimensionB.getGridHeight();
	}
}
