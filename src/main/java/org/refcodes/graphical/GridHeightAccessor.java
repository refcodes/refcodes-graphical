// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a grid height property.
 */
public interface GridHeightAccessor {

	/**
	 * Retrieves the grid height from the grid height property.
	 * 
	 * @return The grid height stored by the grid height property.
	 */
	int getGridHeight();

	/**
	 * Provides a mutator for a grid height property.
	 */
	public interface GridHeightMutator {

		/**
		 * Sets the grid height for the grid height property.
		 * 
		 * @param aHeight The grid height to be stored by the grid height
		 *        property.
		 */
		void setGridHeight( int aHeight );
	}

	/**
	 * Provides a builder method for a grid height property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface GridHeightBuilder<B extends GridHeightBuilder<B>> {

		/**
		 * Sets the grid height for the grid height property.
		 * 
		 * @param aHeight The grid height to be stored by the grid height
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withGridHeight( int aHeight );
	}

	/**
	 * Provides a grid height property.
	 */
	public interface GridHeightProperty extends GridHeightAccessor, GridHeightMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setGridHeight(int)} and returns the very same value (getter).
		 * 
		 * @param aGridHeight The integer to set (via
		 *        {@link #setGridHeight(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letGridHeight( int aGridHeight ) {
			setGridHeight( aGridHeight );
			return aGridHeight;
		}
	}
}
