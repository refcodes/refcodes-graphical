// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a Y offset property.
 */
public interface GridOffset extends GridOffsetXAccessor, GridOffsetYAccessor {

	/**
	 * Provides an accessor for a Y offset property.
	 */
	public interface GridOffsetAccessor extends GridOffset {}

	/**
	 * The Interface GridOffsetMutator.
	 */
	public interface GridOffsetMutator extends GridOffsetXMutator, GridOffsetYMutator {

		/**
		 * Sets the grid offset.
		 *
		 * @param aOffsetX the offset X
		 * @param aOffsetY the offset Y
		 */
		void setGridOffset( int aOffsetX, int aOffsetY );

		/**
		 * Sets the grid offset.
		 *
		 * @param aOffset the new grid offset
		 */
		void setGridOffset( GridOffset aOffset );

		/**
		 * Sets the grid offset.
		 *
		 * @param aOffset the new grid offset
		 */
		void setGridOffset( Offset aOffset );

		/**
		 * Sets the grid offset.
		 *
		 * @param aOffset the new grid offset
		 */
		void setGridOffset( Position aOffset );
	}

	/**
	 * The Interface GridOffsetBuilder.
	 *
	 * @param <B> the generic type
	 */
	public interface GridOffsetBuilder<B extends GridOffsetBuilder<B>> extends GridOffsetXBuilder<B>, GridOffsetYBuilder<B> {

		/**
		 * With grid offset.
		 *
		 * @param aOffsetX the offset X
		 * @param aOffsetY the offset Y
		 * 
		 * @return the b
		 */
		B withGridOffset( int aOffsetX, int aOffsetY );

		/**
		 * With grid offset.
		 *
		 * @param aOffset the offset
		 * 
		 * @return the b
		 */
		B withGridOffset( GridOffset aOffset );

		/**
		 * With grid offset.
		 *
		 * @param aOffset the offset
		 * 
		 * @return the b
		 */
		B withGridOffset( Offset aOffset );

		/**
		 * With grid offset.
		 *
		 * @param aOffset the offset
		 * 
		 * @return the b
		 */
		B withGridOffset( Position aOffset );
	}

	/**
	 * The Interface GridOffsetProperty.
	 */
	public interface GridOffsetProperty extends GridOffsetAccessor, GridOffsetMutator, GridOffsetXProperty, GridOffsetYProperty {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link GridOffset}
		 * (setter) as of {@link #setGridOffset(GridOffset)} and returns the
		 * very same value (getter).
		 * 
		 * @param aGridOffset The {@link GridOffset} to set (via
		 *        {@link #setGridOffset(GridOffset)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default GridOffset letGridOffset( GridOffset aGridOffset ) {
			setGridOffset( aGridOffset );
			return aGridOffset;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Position}
		 * (setter) as of {@link #setGridOffset(Position)} and returns the very
		 * same value (getter).
		 * 
		 * @param aPosition The {@link Position} to set (via
		 *        {@link #setGridOffset(Position)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Position letGridOffset( Position aPosition ) {
			setGridOffset( aPosition );
			return aPosition;
		}

		/**
		 * This method stores and passes through the given arguments, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setGridOffset(int, int)} and returns the very
		 * same values encapsulated as {@link GridOffset} instance.
		 * 
		 * @param aOffsetX The width {@link Dimension} to set (via
		 *        {@link #setGridOffsetX(int)}).
		 * @param aOffsetY The height {@link Dimension} to set (via
		 *        {@link #setGridOffsetY(int)}).
		 * 
		 * @return Returns the values passed encapsulated in a
		 *         {@link GridOffset} object for it to be used in conclusive
		 *         processing steps.
		 */
		default GridOffset letGridOffset( int aOffsetX, int aOffsetY ) {
			setGridOffset( aOffsetX, aOffsetY );
			return new GridOffset() {
				@Override
				public int getGridOffsetX() {
					return aOffsetX;
				}

				@Override
				public int getGridOffsetY() {
					return aOffsetY;
				}
			};
		}
	}

	/**
	 * Equals.
	 *
	 * @param aOffsetA the offset A
	 * @param aOffsetB the offset B
	 * 
	 * @return true, if successful
	 */
	static boolean equals( GridOffset aOffsetA, GridOffset aOffsetB ) {
		return aOffsetA.getGridOffsetX() == aOffsetB.getGridOffsetX() && aOffsetA.getGridOffsetY() == aOffsetB.getGridOffsetY();
	}
}
