// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a X offset property.
 */
public interface GridOffsetXAccessor {

	/**
	 * Retrieves the X offset from the X offset property.
	 * 
	 * @return The X offset stored by the X offset property.
	 */
	int getGridOffsetX();

	/**
	 * Provides a mutator for a X offset property.
	 */
	public interface GridOffsetXMutator {

		/**
		 * Sets the X offset for the X offset property.
		 * 
		 * @param aOffsetX The X offset to be stored by the X offset property.
		 */
		void setGridOffsetX( int aOffsetX );
	}

	/**
	 * Provides a builder method for a X offset property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface GridOffsetXBuilder<B extends GridOffsetXBuilder<B>> {

		/**
		 * Sets the X offset for the X offset property.
		 * 
		 * @param aOffsetX The X offset to be stored by the X offset property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withGridOffsetX( int aOffsetX );
	}

	/**
	 * Provides a X offset property.
	 */
	public interface GridOffsetXProperty extends GridOffsetXAccessor, GridOffsetXMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setGridOffsetX(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aOffsetX The integer to set (via
		 *        {@link #setGridOffsetX(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letGridOffsetX( int aOffsetX ) {
			setGridOffsetX( aOffsetX );
			return aOffsetX;
		}
	}
}
