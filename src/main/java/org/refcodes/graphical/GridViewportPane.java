// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

import org.refcodes.graphical.DragOpacityAccessor.DragOpacityBuilder;
import org.refcodes.graphical.DragOpacityAccessor.DragOpacityProperty;
import org.refcodes.graphical.FieldDimension.FieldDimensionBuilder;
import org.refcodes.graphical.FieldDimension.FieldDimensionProperty;
import org.refcodes.graphical.GridModeAccessor.GridModeBuilder;
import org.refcodes.graphical.GridModeAccessor.GridModeProperty;
import org.refcodes.graphical.MoveModeAccessor.MoveModeBuilder;
import org.refcodes.graphical.MoveModeAccessor.MoveModeProperty;
import org.refcodes.graphical.ViewportDimension.ViewportDimensionBuilder;
import org.refcodes.graphical.ViewportDimension.ViewportDimensionProperty;
import org.refcodes.graphical.ViewportOffset.ViewportOffsetBuilder;
import org.refcodes.graphical.ViewportOffset.ViewportOffsetProperty;

/**
 * The interface {@link GridViewportPane} describes the functionality of a
 * graphical viewport onto a grid (such as a checkerboard): Just the viewport
 * portion of a (larger) grid is displayed according to the settings of this
 * {@link GridViewportPane}. Sprites moved around on the
 * {@link GridViewportPane} are aligned to a virtual grid.
 * 
 *
 * @param <C> the type of the content of the viewport.
 * @param <B> the type of the builder to be returned as of the builder pattern.
 */
public interface GridViewportPane<C, B extends GridViewportPane<C, B>> extends GridModeProperty, GridModeBuilder<B>, FieldDimensionProperty, FieldDimensionBuilder<B>, ViewportOffsetProperty, ViewportOffsetBuilder<B>, ViewportDimensionProperty, ViewportDimensionBuilder<B>, DragOpacityProperty, DragOpacityBuilder<B>, MoveModeProperty, MoveModeBuilder<B> {} // , ContentProperty<C>, ContentBuilder<C, B>
