// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a layout mode property.
 */
public interface LayoutModeAccessor {

	/**
	 * Retrieves the layout mode from the layout mode property.
	 * 
	 * @return The layout mode stored by the horizontal align text mode
	 *         property.
	 */
	LayoutMode getLayoutMode();

	/**
	 * Provides a mutator for a layout mode property.
	 */
	public interface LayoutModeMutator {

		/**
		 * Sets the layout mode for the layout mode property.
		 * 
		 * @param aLayoutMode The layout mode to be stored by the font style
		 *        property.
		 */
		void setLayoutMode( LayoutMode aLayoutMode );
	}

	/**
	 * Provides a builder method for a layout mode property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface LayoutModeBuilder<B extends LayoutModeBuilder<B>> {

		/**
		 * Sets the layout mode for the layout mode property.
		 * 
		 * @param aLayoutMode The layout mode to be stored by the font style
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withLayoutMode( LayoutMode aLayoutMode );
	}

	/**
	 * Provides a layout mode property.
	 */
	public interface LayoutModeProperty extends LayoutModeAccessor, LayoutModeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link LayoutMode}
		 * (setter) as of {@link #setLayoutMode(LayoutMode)} and returns the
		 * very same value (getter).
		 * 
		 * @param aLayoutMode The {@link LayoutMode} to set (via
		 *        {@link #setLayoutMode(LayoutMode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default LayoutMode letLayoutMode( LayoutMode aLayoutMode ) {
			setLayoutMode( aLayoutMode );
			return aLayoutMode;
		}
	}
}
