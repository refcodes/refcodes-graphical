// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a left padding property.
 */
public interface LeftPaddingAccessor {

	/**
	 * Retrieves the left padding from the left padding property.
	 * 
	 * @return The left padding stored by the left padding property.
	 */
	int getLeftPadding();

	/**
	 * Provides a mutator for a left padding property.
	 */
	public interface LeftPaddingMutator {

		/**
		 * Sets the left padding for the left padding property.
		 * 
		 * @param aLeftPadding The left padding to be stored by the left padding
		 *        property.
		 */
		void setLeftPadding( int aLeftPadding );
	}

	/**
	 * Provides a builder method for a left padding property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface LeftPaddingBuilder<B extends LeftPaddingBuilder<B>> {

		/**
		 * Sets the left padding for the left padding property.
		 * 
		 * @param aLeftPadding The left padding to be stored by the left padding
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withLeftPadding( int aLeftPadding );
	}

	/**
	 * Provides a left padding property.
	 */
	public interface LeftPaddingProperty extends LeftPaddingAccessor, LeftPaddingMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setLeftPadding(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aLeftPadding The integer to set (via
		 *        {@link #setLeftPadding(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letLeftPadding( int aLeftPadding ) {
			setLeftPadding( aLeftPadding );
			return aLeftPadding;
		}
	}
}
