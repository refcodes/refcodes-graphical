// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a max grid dimension property.
 */
public interface MaxGridDimension {

	/**
	 * Retrieves the max grid dimension from the max grid dimension property.
	 * 
	 * @return The max grid dimension stored by the max grid dimension property.
	 */
	GridDimension getMaxGridDimension();

	/**
	 * Provides a mutator for a max grid dimension property.
	 */
	public interface MaxGridDimensionMutator {

		/**
		 * Sets the max grid dimension for the max grid dimension property.
		 * 
		 * @param aDimension The max grid dimension to be stored by the grid
		 *        dimension property.
		 */
		void setMaxGridDimension( Dimension aDimension );

		/**
		 * Sets the max grid dimension for the max grid dimension property.
		 * 
		 * @param aGridDimension The max grid dimension to be stored by the grid
		 *        dimension property.
		 */
		void setMaxGridDimension( GridDimension aGridDimension );

		/**
		 * Sets the max grid dimension for the max grid dimension property.
		 * 
		 * @param aWidth The max grid width to be stored by the max grid
		 *        dimension property.
		 * @param aHeight The max grid height to be stored by the max grid
		 *        dimension property.
		 */
		void setMaxGridDimension( int aWidth, int aHeight );
	}

	/**
	 * Provides a builder method for a max grid dimension property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface MaxGridDimensionBuilder<B extends MaxGridDimensionBuilder<B>> {

		/**
		 * Sets the max grid dimension for the max grid dimension property.
		 * 
		 * @param aDimension The max grid dimension to be stored by the grid
		 *        dimension property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMaxGridDimension( Dimension aDimension );

		/**
		 * Sets the max grid dimension for the max grid dimension property.
		 * 
		 * @param aGridDimension The max grid dimension to be stored by the grid
		 *        dimension property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMaxGridDimension( GridDimension aGridDimension );

		/**
		 * Sets the max grid dimension for the max grid dimension property.
		 * 
		 * @param aWidth The max grid width to be stored by the max grid
		 *        dimension property.
		 * @param aHeight The max grid height to be stored by the max grid
		 *        dimension property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMaxGridDimension( int aWidth, int aHeight );

	}

	/**
	 * Provides a max grid dimension property.
	 */
	public interface MaxGridDimensionProperty extends MaxGridDimension, MaxGridDimensionMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setMaxGridDimension(GridDimension)} and
		 * returns the very same value (getter).
		 * 
		 * @param aMaxGridDimension The {@link Dimension} to set (via
		 *        {@link #setMaxGridDimension(Dimension)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Dimension letMaxGridDimension( Dimension aMaxGridDimension ) {
			setMaxGridDimension( aMaxGridDimension );
			return aMaxGridDimension;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link GridDimension}
		 * (setter) as of {@link #setMaxGridDimension(GridDimension)} and
		 * returns the very same value (getter).
		 * 
		 * @param aMaxGridDimension The {@link GridDimension} to set (via
		 *        {@link #setMaxGridDimension(GridDimension)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default GridDimension letMaxGridDimension( GridDimension aMaxGridDimension ) {
			setMaxGridDimension( aMaxGridDimension );
			return aMaxGridDimension;
		}

		/**
		 * This method stores and passes through the given arguments, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setMaxGridDimension(int, int)} and returns the
		 * very same values encapsulated as {@link GridDimension} instance.
		 * 
		 * @param aWidth The width {@link Dimension} to set.
		 * @param aHeight The height {@link Dimension} to set.
		 * 
		 * @return Returns the values passed encapsulated in a
		 *         {@link GridDimension} object for it to be used in conclusive
		 *         processing steps.
		 */
		default GridDimension letMaxGridDimension( int aWidth, int aHeight ) {
			setMaxGridDimension( aWidth, aHeight );
			return getMaxGridDimension();
		}
	}
}
