// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a max viewport dimension property.
 */
public interface MaxViewportDimension {

	/**
	 * Retrieves the max viewport dimension from the max viewport dimension
	 * property.
	 * 
	 * @return The max viewport dimension stored by the max viewport dimension
	 *         property.
	 */
	ViewportDimension getMaxViewportDimension();

	/**
	 * Provides a mutator for a max viewport dimension property.
	 */
	public interface MaxViewportDimensionMutator {

		/**
		 * Sets the max viewport dimension for the max viewport dimension
		 * property.
		 * 
		 * @param aDimension The max viewport dimension to be stored by the
		 *        viewport dimension property.
		 */
		void setMaxViewportDimension( Dimension aDimension );

		/**
		 * Sets the max viewport dimension for the max viewport dimension
		 * property.
		 * 
		 * @param aDimension The max viewport dimension to be stored by the
		 *        viewport dimension property.
		 */
		void setMaxViewportDimension( ViewportDimension aDimension );

		/**
		 * Sets the max viewport dimension for the max viewport dimension
		 * property.
		 * 
		 * @param aWidth The max viewport width to be stored by the max viewport
		 *        dimension property.
		 * @param aHeight The max viewport height to be stored by the max
		 *        viewport dimension property.
		 */
		void setMaxViewportDimension( int aWidth, int aHeight );
	}

	/**
	 * Provides a builder method for a max viewport dimension property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface MaxViewportDimensionBuilder<B extends MaxViewportDimensionBuilder<B>> {

		/**
		 * Sets the max viewport dimension for the max viewport dimension
		 * property.
		 * 
		 * @param aWidth The max viewport width to be stored by the max viewport
		 *        dimension property.
		 * @param aHeight The max viewport height to be stored by the max
		 *        viewport dimension property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMaxViewportDimension( int aWidth, int aHeight );

		/**
		 * Sets the max viewport dimension for the max viewport dimension
		 * property.
		 * 
		 * @param aDimension The max viewport dimension to be stored by the
		 *        viewport dimension property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMaxViewportDimension( Dimension aDimension );

		/**
		 * Sets the max viewport dimension for the max viewport dimension
		 * property.
		 * 
		 * @param aDimension The max viewport dimension to be stored by the
		 *        viewport dimension property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMaxViewportDimension( ViewportDimension aDimension );
	}

	/**
	 * Provides a max viewport dimension property builder.
	 */
	public interface MaxViewportDimensionProperty extends MaxViewportDimension, MaxViewportDimensionMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setMaxViewportDimension(ViewportDimension)}
		 * and returns the very same value (getter).
		 * 
		 * @param aMaxViewportDimension The {@link Dimension} to set (via
		 *        {@link #setMaxViewportDimension(Dimension)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Dimension letMaxViewportDimension( Dimension aMaxViewportDimension ) {
			setMaxViewportDimension( aMaxViewportDimension );
			return aMaxViewportDimension;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link ViewportDimension} (setter) as of
		 * {@link #setMaxViewportDimension(ViewportDimension)} and returns the
		 * very same value (getter).
		 * 
		 * @param aMaxViewportDimension The {@link ViewportDimension} to set
		 *        (via {@link #setMaxViewportDimension(ViewportDimension)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ViewportDimension letMaxViewportDimension( ViewportDimension aMaxViewportDimension ) {
			setMaxViewportDimension( aMaxViewportDimension );
			return aMaxViewportDimension;
		}

		/**
		 * This method stores and passes through the given arguments, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setMaxViewportDimension(int, int)} and returns
		 * the very same values encapsulated as {@link ViewportDimension}
		 * instance.
		 * 
		 * @param aWidth The width {@link Dimension} to set.
		 * @param aHeight The height {@link Dimension} to set.
		 * 
		 * @return Returns the values passed encapsulated in a
		 *         {@link ViewportDimension} object for it to be used in
		 *         conclusive processing steps.
		 */
		default ViewportDimension letMaxViewportDimension( int aWidth, int aHeight ) {
			setMaxViewportDimension( aWidth, aHeight );
			return getMaxViewportDimension();
		}
	}
}
