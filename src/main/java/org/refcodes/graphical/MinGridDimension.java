// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a min grid dimension property.
 */
public interface MinGridDimension {

	/**
	 * Retrieves the min grid dimension from the min grid dimension property.
	 * 
	 * @return The min grid dimension stored by the min grid dimension property.
	 */
	GridDimension getMinGridDimension();

	/**
	 * Provides a mutator for a min grid dimension property.
	 */
	public interface MinGridDimensionMutator {

		/**
		 * Sets the min grid dimension for the min grid dimension property.
		 * 
		 * @param aGridDimension The min grid dimension to be stored by the grid
		 *        dimension property.
		 */
		void setMinGridDimension( GridDimension aGridDimension );

		/**
		 * Sets the min grid dimension for the min grid dimension property.
		 * 
		 * @param aDimension The min grid dimension to be stored by the grid
		 *        dimension property.
		 */
		void setMinGridDimension( Dimension aDimension );

		/**
		 * Sets the min grid dimension for the min grid dimension property.
		 * 
		 * @param aGridWidth The min grid width to be stored by the min grid
		 *        dimension property.
		 * @param aGridHeight The min grid height to be stored by the min grid
		 *        dimension property.
		 */
		void setMinGridDimension( int aGridWidth, int aGridHeight );

	}

	/**
	 * Provides a builder method for a min grid dimension property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface MinGridDimensionBuilder<B extends MinGridDimensionBuilder<B>> {

		/**
		 * Sets the min grid dimension for the min grid dimension property.
		 * 
		 * @param aGridWidth The min grid width to be stored by the min grid
		 *        dimension property.
		 * @param aGridHeight The min grid height to be stored by the min grid
		 *        dimension property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMinGridDimension( int aGridWidth, int aGridHeight );

		/**
		 * Sets the min grid dimension for the min grid dimension property.
		 * 
		 * @param aDimension The min grid dimension to be stored by the min grid
		 *        dimension property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMinGridDimension( Dimension aDimension );

		/**
		 * Sets the min grid dimension for the min grid dimension property.
		 * 
		 * @param aGridDimension The min grid dimension to be stored by the min
		 *        grid dimension property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMinGridDimension( GridDimension aGridDimension );

	}

	/**
	 * Provides a min grid dimension property.
	 */
	public interface MinGridDimensionProperty extends MinGridDimension, MinGridDimensionMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setMinGridDimension(GridDimension)} and
		 * returns the very same value (getter).
		 * 
		 * @param aMinGridDimension The {@link Dimension} to set (via
		 *        {@link #setMinGridDimension(Dimension)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Dimension letMinGridDimension( Dimension aMinGridDimension ) {
			setMinGridDimension( aMinGridDimension );
			return aMinGridDimension;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link GridDimension}
		 * (setter) as of {@link #setMinGridDimension(GridDimension)} and
		 * returns the very same value (getter).
		 * 
		 * @param aMinGridDimension The {@link GridDimension} to set (via
		 *        {@link #setMinGridDimension(GridDimension)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default GridDimension letMinGridDimension( GridDimension aMinGridDimension ) {
			setMinGridDimension( aMinGridDimension );
			return aMinGridDimension;
		}

		/**
		 * This method stores and passes through the given arguments, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setMinGridDimension(int, int)} and returns the
		 * very same values encapsulated as {@link GridDimension} instance.
		 * 
		 * @param aWidth The width {@link Dimension} to set.
		 * @param aHeight The height {@link Dimension} to set.
		 * 
		 * @return Returns the values passed encapsulated in a
		 *         {@link GridDimension} object for it to be used in conclusive
		 *         processing steps.
		 */
		default GridDimension letMinGridDimension( int aWidth, int aHeight ) {
			setMinGridDimension( aWidth, aHeight );
			return getMinGridDimension();
		}
	}
}
