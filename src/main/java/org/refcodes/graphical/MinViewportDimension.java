// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a min viewport dimension property.
 */
public interface MinViewportDimension {

	/**
	 * Retrieves the min viewport dimension from the min viewport dimension
	 * property.
	 * 
	 * @return The min viewport dimension stored by the min viewport dimension
	 *         property.
	 */
	ViewportDimension getMinViewportDimension();

	/**
	 * Provides a mutator for a min viewport dimension property.
	 */
	public interface MinViewportDimensionMutator {

		/**
		 * Sets the min viewport dimension for the min viewport dimension
		 * property.
		 * 
		 * @param aDimension The min viewport dimension to be stored by the
		 *        viewport dimension property.
		 */
		void setMinViewportDimension( Dimension aDimension );

		/**
		 * Sets the min viewport dimension for the min viewport dimension
		 * property.
		 * 
		 * @param aDimension The min viewport dimension to be stored by the
		 *        viewport dimension property.
		 */
		void setMinViewportDimension( ViewportDimension aDimension );

		/**
		 * Sets the min viewport dimension for the min viewport dimension
		 * property.
		 * 
		 * @param aWidth The min viewport width to be stored by the min viewport
		 *        dimension property.
		 * @param aHeight The min viewport height to be stored by the min
		 *        viewport dimension property.
		 */
		void setMinViewportDimension( int aWidth, int aHeight );

	}

	/**
	 * Provides a builder method for a min viewport dimension property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface MinViewportDimensionBuilder<B extends MinViewportDimensionBuilder<B>> {

		/**
		 * Sets the min viewport dimension for the min viewport dimension
		 * property.
		 * 
		 * @param aWidth The min viewport width to be stored by the min viewport
		 *        dimension property.
		 * @param aHeight The min viewport height to be stored by the min
		 *        viewport dimension property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMinViewportDimension( int aWidth, int aHeight );

		/**
		 * Sets the min viewport dimension for the min viewport dimension
		 * property.
		 * 
		 * @param aDimension The min viewport dimension to be stored by the min
		 *        viewport dimension property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMinViewportDimension( Dimension aDimension );

		/**
		 * Sets the min viewport dimension for the min viewport dimension
		 * property.
		 * 
		 * @param aDimension The min viewport dimension to be stored by the min
		 *        viewport dimension property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMinViewportDimension( ViewportDimension aDimension );
	}

	/**
	 * Provides a min viewport dimension property.
	 */
	public interface MinViewportDimensionProperty extends MinViewportDimension, MinViewportDimensionMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setMinViewportDimension(ViewportDimension)}
		 * and returns the very same value (getter).
		 * 
		 * @param aMinViewportDimension The {@link Dimension} to set (via
		 *        {@link #setMinViewportDimension(Dimension)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Dimension letMinViewportDimension( Dimension aMinViewportDimension ) {
			setMinViewportDimension( aMinViewportDimension );
			return aMinViewportDimension;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link ViewportDimension} (setter) as of
		 * {@link #setMinViewportDimension(ViewportDimension)} and returns the
		 * very same value (getter).
		 * 
		 * @param aMinViewportDimension The {@link ViewportDimension} to set
		 *        (via {@link #setMinViewportDimension(ViewportDimension)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ViewportDimension letMinViewportDimension( ViewportDimension aMinViewportDimension ) {
			setMinViewportDimension( aMinViewportDimension );
			return aMinViewportDimension;
		}

		/**
		 * This method stores and passes through the given arguments, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setMinViewportDimension(int, int)} and returns
		 * the very same values encapsulated as {@link ViewportDimension}
		 * instance.
		 * 
		 * @param aWidth The width {@link Dimension} to set.
		 * @param aHeight The height {@link Dimension} to set.
		 * 
		 * @return Returns the values passed encapsulated in a
		 *         {@link ViewportDimension} object for it to be used in
		 *         conclusive processing steps.
		 */
		default ViewportDimension letMinViewportDimension( int aWidth, int aHeight ) {
			setMinViewportDimension( aWidth, aHeight );
			return getMinViewportDimension();
		}
	}
}
