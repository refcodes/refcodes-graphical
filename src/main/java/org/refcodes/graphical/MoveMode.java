/*
 * 
 */
package org.refcodes.graphical;

/**
 * The Enum MoveMode.
 */
public enum MoveMode {

	SMOOTH,

	JUMPY
}