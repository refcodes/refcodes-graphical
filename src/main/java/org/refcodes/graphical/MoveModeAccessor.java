// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a move mode property.
 */
public interface MoveModeAccessor {

	/**
	 * Retrieves the move mode from the move mode property.
	 * 
	 * @return The move mode stored by the move mode property.
	 */
	MoveMode getMoveMode();

	/**
	 * Provides a mutator for a move mode property.
	 */
	public interface MoveModeMutator {

		/**
		 * Sets the move mode for the move mode property.
		 * 
		 * @param aMoveMode The move mode to be stored by the move mode
		 *        property.
		 */
		void setMoveMode( MoveMode aMoveMode );
	}

	/**
	 * Provides a builder method for a move mode property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface MoveModeBuilder<B extends MoveModeBuilder<B>> {

		/**
		 * Sets the move mode for the move mode property.
		 * 
		 * @param aMoveMode The move mode to be stored by the move mode
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMoveMode( MoveMode aMoveMode );
	}

	/**
	 * Provides a move mode property.
	 */
	public interface MoveModeProperty extends MoveModeAccessor, MoveModeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link MoveMode}
		 * (setter) as of {@link #setMoveMode(MoveMode)} and returns the very
		 * same value (getter).
		 * 
		 * @param aMoveMode The {@link MoveMode} to set (via
		 *        {@link #setMoveMode(MoveMode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default MoveMode letMoveMode( MoveMode aMoveMode ) {
			setMoveMode( aMoveMode );
			return aMoveMode;
		}
	}
}
