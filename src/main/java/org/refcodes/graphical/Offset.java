// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a Y offset property.
 */
public interface Offset extends OffsetXAccessor, OffsetYAccessor {

	/**
	 * Provides an accessor for a Y offset property.
	 */
	public interface OffsetAccessor extends Offset {}

	/**
	 * The Interface OffsetMutator.
	 */
	public interface OffsetMutator extends OffsetXMutator, OffsetYMutator {

		/**
		 * Sets the offset.
		 *
		 * @param aOffsetX the offset X
		 * @param aOffsetY the offset Y
		 */
		void setOffset( int aOffsetX, int aOffsetY );

		/**
		 * Sets the offset.
		 *
		 * @param aOffset the new offset
		 */
		void setOffset( Offset aOffset );

		/**
		 * Sets the offset.
		 *
		 * @param aOffset the new offset
		 */
		void setOffset( Position aOffset );
	}

	/**
	 * The Interface OffsetBuilder.
	 *
	 * @param <B> the generic type
	 */
	public interface OffsetBuilder<B extends OffsetBuilder<B>> extends OffsetXBuilder<B>, OffsetYBuilder<B> {

		/**
		 * With offset.
		 *
		 * @param aOffsetX the offset X
		 * @param aOffsetY the offset Y
		 * 
		 * @return the b
		 */
		B withOffset( int aOffsetX, int aOffsetY );

		/**
		 * With offset.
		 *
		 * @param aOffset the offset
		 * 
		 * @return the b
		 */
		B withOffset( Offset aOffset );

		/**
		 * With offset.
		 *
		 * @param aOffset the offset
		 * 
		 * @return the b
		 */
		B withOffset( Position aOffset );
	}

	/**
	 * The Interface OffsetProperty.
	 */
	public interface OffsetProperty extends OffsetAccessor, OffsetMutator, OffsetXProperty, OffsetYProperty {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Offset} (setter)
		 * as of {@link #setOffset(Offset)} and returns the very same value
		 * (getter).
		 * 
		 * @param aOffset The {@link Offset} to set (via
		 *        {@link #setOffset(Offset)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Offset letOffset( Offset aOffset ) {
			setOffset( aOffset );
			return aOffset;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Position}
		 * (setter) as of {@link #setOffset(Position)} and returns the very same
		 * value (getter).
		 * 
		 * @param aPosition The {@link Position} to set (via
		 *        {@link #setOffset(Position)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Position letOffset( Position aPosition ) {
			setOffset( aPosition );
			return aPosition;
		}

		/**
		 * This method stores and passes through the given arguments, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setOffset(int, int)} and returns the very same
		 * values encapsulated as {@link Offset} instance.
		 * 
		 * @param aOffsetX The width {@link Dimension} to set (via
		 *        {@link #setOffsetX(int)}).
		 * @param aOffsetY The height {@link Dimension} to set (via
		 *        {@link #setOffsetY(int)}).
		 * 
		 * @return Returns the values passed encapsulated in a {@link Offset}
		 *         object for it to be used in conclusive processing steps.
		 */
		default Offset letOffset( int aOffsetX, int aOffsetY ) {
			setOffset( aOffsetX, aOffsetY );
			return new Offset() {
				@Override
				public int getOffsetX() {
					return aOffsetX;
				}

				@Override
				public int getOffsetY() {
					return aOffsetY;
				}
			};
		}
	}

	/**
	 * Equals.
	 *
	 * @param aOffsetA the offset A
	 * @param aOffsetB the offset B
	 * 
	 * @return true, if successful
	 */
	static boolean equals( Offset aOffsetA, Offset aOffsetB ) {
		return aOffsetA.getOffsetX() == aOffsetB.getOffsetX() && aOffsetA.getOffsetY() == aOffsetB.getOffsetY();
	}
}
