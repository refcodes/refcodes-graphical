// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a opacity property.
 */
public interface OpacityAccessor {

	/**
	 * Retrieves the opacity from the opacity property.
	 * 
	 * @return The opacity stored by the opacity property.
	 */
	double getOpacity();

	/**
	 * Provides a mutator for a opacity property.
	 */
	public interface OpacityMutator {

		/**
		 * Sets the opacity for the opacity property.
		 * 
		 * @param aOpacity The opacity to be stored by the opacity property.
		 */
		void setOpacity( double aOpacity );
	}

	/**
	 * Provides a builder method for a opacity property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface OpacityBuilder<B extends OpacityBuilder<B>> {

		/**
		 * Sets the opacity for the opacity property.
		 * 
		 * @param aOpacity The opacity to be stored by the opacity property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withOpacity( double aOpacity );
	}

	/**
	 * Provides a opacity property.
	 */
	public interface OpacityProperty extends OpacityAccessor, OpacityMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given double (setter) as of
		 * {@link #setOpacity(double)} and returns the very same value (getter).
		 * 
		 * @param aOpacity The double to set (via {@link #setOpacity(double)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default double letOpacity( double aOpacity ) {
			setOpacity( aOpacity );
			return aOpacity;
		}
	}
}
