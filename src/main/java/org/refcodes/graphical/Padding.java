// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a padding property.
 */
public interface Padding extends LeftPaddingAccessor, RightPaddingAccessor, TopPaddingAccessor, BottomPaddingAccessor {

	/**
	 * Provides an accessor for a padding property.
	 */
	public interface PaddingAccessor extends Padding {}

	/**
	 * The Interface PaddingMutator.
	 */
	public interface PaddingMutator extends LeftPaddingMutator, RightPaddingMutator, TopPaddingMutator, BottomPaddingMutator {

		/**
		 * Sets the padding.
		 *
		 * @param aTopPadding the top padding
		 * @param aRightPadding the right padding
		 * @param aBottomPadding the bottom padding
		 * @param aLeftPadding the left padding
		 */
		void setPadding( int aTopPadding, int aRightPadding, int aBottomPadding, int aLeftPadding );

		/**
		 * Sets the padding.
		 *
		 * @param aPadding the new padding
		 */
		void setPadding( Padding aPadding );
	}

	/**
	 * The Interface PaddingBuilder.
	 *
	 * @param <B> the generic type
	 */
	public interface PaddingBuilder<B extends PaddingBuilder<B>> extends LeftPaddingBuilder<B>, RightPaddingBuilder<B>, TopPaddingBuilder<B>, BottomPaddingBuilder<B> {

		/**
		 * With padding.
		 *
		 * @param aTopPadding the top padding
		 * @param aRightPadding the padding Y
		 * @param aBottomPadding the bottom padding
		 * @param aLeftPadding the padding X
		 * 
		 * @return the b
		 */
		B withPadding( int aTopPadding, int aRightPadding, int aBottomPadding, int aLeftPadding );

		/**
		 * With padding.
		 *
		 * @param aPadding the padding
		 * 
		 * @return the b
		 */
		B withPadding( Padding aPadding );
	}

	/**
	 * The Interface PaddingProperty.
	 */
	public interface PaddingProperty extends PaddingAccessor, PaddingMutator, LeftPaddingProperty, RightPaddingProperty, TopPaddingProperty, BottomPaddingProperty {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Padding} (setter)
		 * as of {@link #setPadding(Padding)} and returns the very same value
		 * (getter).
		 * 
		 * @param aPadding The {@link Padding} to set (via
		 *        {@link #setPadding(Padding)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Padding letPadding( Padding aPadding ) {
			setPadding( aPadding );
			return aPadding;
		}

		/**
		 * This method stores and passes through the given arguments, which is
		 * very useful for builder APIs.
		 *
		 * @param aTopPadding the top padding
		 * @param aRightPadding the right padding
		 * @param aBottomPadding the bottom padding
		 * @param aLeftPadding the left padding
		 * 
		 * @return the padding accessor
		 */
		default Padding letPadding( int aTopPadding, int aRightPadding, int aBottomPadding, int aLeftPadding ) {
			setPadding( aTopPadding, aRightPadding, aBottomPadding, aLeftPadding );
			return new Padding() {
				@Override
				public int getLeftPadding() {
					return aLeftPadding;
				}

				@Override
				public int getRightPadding() {
					return aRightPadding;
				}

				@Override
				public int getTopPadding() {
					return aTopPadding;
				}

				@Override
				public int getBottomPadding() {
					return aBottomPadding;
				}
			};
		}
	}

	/**
	 * Equals.
	 *
	 * @param aPaddingA the padding A
	 * @param aPaddingB the padding B
	 * 
	 * @return true, if successful
	 */
	static boolean equals( Padding aPaddingA, Padding aPaddingB ) {
		return aPaddingA.getLeftPadding() == aPaddingB.getLeftPadding() && aPaddingA.getRightPadding() == aPaddingB.getRightPadding() && aPaddingA.getTopPadding() == aPaddingB.getTopPadding() && aPaddingA.getBottomPadding() == aPaddingB.getBottomPadding();
	}
}
