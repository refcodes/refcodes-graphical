/*
 * 
 */
package org.refcodes.graphical;

import org.refcodes.graphical.RgbPixmap.RgbPixmapBuilder;

/**
 * The {@link PixGridBannerPanel} extends the {@link PixGridPane} and manages a
 * {@link RgbPixmap} to be painted on a grid panel (such as an LED matrix) and
 * moved or faded over that grid panel. The panel is divided into pixels of a
 * defined x and y position, a defined size and defined {@link PixelShape}. The
 * underlying {@link RgbPixmap} is projected onto that grid pane by applying any
 * of the various operations.
 */
public interface PixGridBannerPanel extends PixGridPane {

	/**
	 * Move the {@link RgbPixmap} to the north.
	 *
	 * @param aSteps the a steps
	 * @param aStartDelayMillis the a start delay in milliseconds
	 * @param aEndDelayMillis the a end delay in milliseconds
	 */
	void moveNorth( int aSteps, int aStartDelayMillis, int aEndDelayMillis );

	/**
	 * Move the {@link RgbPixmap} to the north east.
	 *
	 * @param aSteps the a steps
	 * @param aStartDelayMillis the a start delay in milliseconds
	 * @param aEndDelayMillis the a end delay in milliseconds
	 */
	void moveNorthEast( int aSteps, int aStartDelayMillis, int aEndDelayMillis );

	/**
	 * Move the {@link RgbPixmap} to the east.
	 *
	 * @param aSteps the a steps
	 * @param aStartDelayMillis the a start delay in milliseconds
	 * @param aEndDelayMillis the a end delay in milliseconds
	 */
	void moveEast( int aSteps, int aStartDelayMillis, int aEndDelayMillis );

	/**
	 * Move the {@link RgbPixmap} to the south east.
	 *
	 * @param aSteps the a steps
	 * @param aStartDelayMillis the a start delay in milliseconds
	 * @param aEndDelayMillis the a end delay in milliseconds
	 */
	void moveSouthEast( int aSteps, int aStartDelayMillis, int aEndDelayMillis );

	/**
	 * Move the {@link RgbPixmap} to the south.
	 *
	 * @param aSteps the a steps
	 * @param aStartDelayMillis the a start delay in milliseconds
	 * @param aEndDelayMillis the a end delay in milliseconds
	 */
	void moveSouth( int aSteps, int aStartDelayMillis, int aEndDelayMillis );

	/**
	 * Move the {@link RgbPixmap} to the south west.
	 *
	 * @param aSteps the a steps
	 * @param aStartDelayMillis the a start delay in milliseconds
	 * @param aEndDelayMillis the a end delay in milliseconds
	 */
	void moveSouthWest( int aSteps, int aStartDelayMillis, int aEndDelayMillis );

	/**
	 * Move the {@link RgbPixmap} to the west.
	 *
	 * @param aSteps the a steps
	 * @param aStartDelayMillis the a start delay in milliseconds
	 * @param aEndDelayMillis the a end delay in milliseconds
	 */
	void moveWest( int aSteps, int aStartDelayMillis, int aEndDelayMillis );

	/**
	 * Move the {@link RgbPixmap} to the north west.
	 *
	 * @param aSteps the a steps
	 * @param aStartDelayMillis the a start delay in milliseconds
	 * @param aEndDelayMillis the a end delay in milliseconds
	 */
	void moveNorthWest( int aSteps, int aStartDelayMillis, int aEndDelayMillis );

	/**
	 * Step fade to pixmap.
	 *
	 * @param aFadeToPixmap the a fade to pixmap
	 * @param aOffsetX the a offset X
	 * @param aOffsetY the a offset Y
	 * @param aStepWidth the a step width
	 * @param aPixelDelayMillis the a pixel delay milliseconds
	 */
	void stepFadeToPixmap( RgbPixmapBuilder aFadeToPixmap, int aOffsetX, int aOffsetY, int aStepWidth, int aPixelDelayMillis );

	/**
	 * Rnd fade to pixmap.
	 *
	 * @param aFadeToPixmap the a fade to pixmap
	 * @param aOffsetX the a offset X
	 * @param aOffsetY the a offset Y
	 * @param aPixelPropability the a pixel probability
	 * @param aPixelDelayMillis the a pixel delay milliseconds
	 */
	void rndFadeToPixmap( RgbPixmapBuilder aFadeToPixmap, int aOffsetX, int aOffsetY, float aPixelPropability, int aPixelDelayMillis );
}
