/*
 * 
 */
package org.refcodes.graphical;

import org.refcodes.graphical.RgbPixmap.RgbPixmapBuilder;
import org.refcodes.graphical.VisibleAccessor.VisibleProperty;
import org.refcodes.mixin.Clearable;

/**
 * The {@link PixGridPane} projects a {@link RgbPixmap} on a grid panel (such as
 * an LED matrix) , the underlying {@link RgbPixmap} is painted onto that grid
 * pane as implemented by any implementing classes.
 */
public interface PixGridPane extends VisibleProperty, Clearable {

	/**
	 * Gets the {@link RgbPixel} at the given position.
	 *
	 * @param aPosX the x position
	 * @param aPosY the y position
	 * 
	 * @return the {@link RgbPixel} at the given position
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	RgbPixel getPixelAt( int aPosX, int aPosY );

	/**
	 * Paints the {@link RgbPixel} at the given position.
	 *
	 * @param aPixel the {@link RgbPixel} for the pixel at the given position.
	 * @param aPosX the x position
	 * @param aPosY the y position
	 */
	void setPixelAtAt( RgbPixel aPixel, int aPosX, int aPosY );

	/**
	 * Gets the top border.
	 *
	 * @return the top border
	 */
	int getTopBorder();

	/**
	 * Gets the bottom border.
	 *
	 * @return the bottom border
	 */
	int getBottomBorder();

	/**
	 * Gets the left border.
	 *
	 * @return the left border
	 */
	int getLeftBorder();

	/**
	 * Gets the right border.
	 *
	 * @return the right border
	 */
	int getRightBorder();

	/**
	 * Gets the horizontal space.
	 *
	 * @return the horizontal space
	 */
	int getHorizontalSpace();

	/**
	 * Gets the vertical space.
	 *
	 * @return the vertical space
	 */
	int getVerticalSpace();

	/**
	 * Gets the pixel width.
	 *
	 * @return the pixel width
	 */
	int getPixelWidth();

	/**
	 * Gets the pixel height.
	 *
	 * @return the pixel height
	 */
	int getPixelHeight();

	/**
	 * Gets the pixel shape.
	 *
	 * @return the pixel shape
	 */
	PixelShape getPixelShape();

	/**
	 * Gets the matrix width.
	 *
	 * @return the matrix width
	 */
	int getMatrixWidth();

	/**
	 * Gets the matrix height.
	 *
	 * @return the matrix height
	 */
	int getMatrixHeight();

	/**
	 * Gets the matrix size.
	 *
	 * @return the matrix size
	 */
	Dimension getMatrixSize();

	/**
	 * Gets the underlying {@link RgbPixmapBuilder}.
	 *
	 * @return the according {@link RgbPixmapBuilder}.
	 */
	RgbPixmapBuilder getRgbPixmap();

	/**
	 * Gets the pixmap offset X.
	 *
	 * @return the pixmap offset X
	 */
	int getPixmapOffsetX();

	/**
	 * Gets the pixmap offset Y.
	 *
	 * @return the pixmap offset Y
	 */
	int getPixmapOffsetY();

	/**
	 * Checks if is horizontal wrap enabled.
	 *
	 * @return true, if is horizontal wrap enabled
	 */
	boolean isHorizontalWrapEnabled();

	/**
	 * Checks if is vertical wrap enabled.
	 *
	 * @return true, if is vertical wrap enabled
	 */
	boolean isVerticalWrapEnabled();

	/**
	 * Gets the pixmap snapshot.
	 *
	 * @return the pixmap snapshot
	 */
	RgbPixmapBuilder getPixmapSnapshot();

	/**
	 * Adds the blank area not to be painted with grid pixels.
	 *
	 * @param aPosX the x position
	 * @param aPosY the y position
	 * @param aWidth the according area's width
	 * @param aHeight the according area's height
	 * 
	 * @return true, if successfully added, false if such an area has already
	 *         been added.
	 */
	boolean addBlankArea( int aPosX, int aPosY, int aWidth, int aHeight );

	/**
	 * Adds the blank area not to be painted with grid pixels.
	 *
	 * @param aArea the {@link Rectangle} representing the blank area.
	 * 
	 * @return true, if successfully added, false if such an area has already
	 *         been added.
	 */
	boolean addBlankArea( Rectangle aArea );

	/**
	 * Removes the blank area now to be painted with grid pixels again.
	 *
	 * @param aArea the {@link Rectangle} representing the blank area.
	 * 
	 * @return true, if successfully removed, false if such an area has not been
	 *         added before.
	 */
	boolean removeBlankArea( Rectangle aArea );

	/**
	 * Removes all blank areas now to be painted with grid pixels again.
	 */
	void clearBlankAreas();

	/**
	 * Gets the inactive pixel color.
	 *
	 * @return the inactive pixel color
	 */
	RgbColor getInactivePixelColor();

	/**
	 * Gets the matrix drawing width.
	 *
	 * @return the matrix drawing width
	 */
	default int getMatrixDrawingWidth() {
		return getMatrixDrawingWidth( true, true, true );
	}

	/**
	 * Gets the matrix drawing height.
	 *
	 * @return the matrix drawing height
	 */
	default int getMatrixDrawingHeight() {
		return getMatrixDrawingHeight( true, true, true );
	}

	/**
	 * Gets the matrix drawing width.
	 *
	 * @param isWithBorder the is with border
	 * @param isWithPixelSpace the is with pixel space
	 * @param isWithPixelWidth the is with pixel width
	 * 
	 * @return the matrix drawing width
	 */
	int getMatrixDrawingWidth( boolean isWithBorder, boolean isWithPixelSpace, boolean isWithPixelWidth );

	/**
	 * Gets the matrix drawing height.
	 *
	 * @param isWithBorder the is with border
	 * @param isWithPixelSpace the is with pixel space
	 * @param isWithPixelHeight the is with pixel height
	 * 
	 * @return the matrix drawing height
	 */
	int getMatrixDrawingHeight( boolean isWithBorder, boolean isWithPixelSpace, boolean isWithPixelHeight );

	/**
	 * Sets the pixmap offset.
	 *
	 * @param aPosX the a X pos
	 * @param aPosY the a Y pos
	 */
	void setPixmapOffset( int aPosX, int aPosY );

	/**
	 * Repaint the pane with the underlying metrics and pixmap.
	 */
	void repaint();
}
