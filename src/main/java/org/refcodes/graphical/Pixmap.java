// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

import java.awt.Color;

/**
 * A pixmap represents the data of a two dimensional image. The pixels may be
 * accessed by providing an x and a y position and, depending on the sub-types,
 * a pixel my be represented by an {@link RgbPixel} (as of the {@link Pixmap}
 * type).
 * 
 * @param <PX> The type of the pixel being managed by the {@link Pixmap}.
 */
public interface Pixmap<PX> extends WidthAccessor, HeightAccessor {

	/**
	 * Returns the two dimensional pixels array representing the pixmap.
	 * 
	 * @return The pixmap array.
	 */
	PX[][] getPixels();

	/**
	 * Retrieves the width of the {@link Pixmap}.
	 * 
	 * @return The width of the {@link Pixmap}
	 */
	@Override
	int getWidth();

	/**
	 * Retrieves the height of the {@link Pixmap}.
	 * 
	 * @return The height of the {@link Pixmap}
	 */
	@Override
	int getHeight();

	/**
	 * Retrieves a pixel at a given position.
	 * 
	 * @param aPosX The x position for the pixel to be retrieved.
	 * @param aPosY The y position for the pixel to be retrieved.
	 * 
	 * @return The according pixel
	 * 
	 * @throws IndexOutOfBoundsException in case the index is out of bounds.
	 */
	PX getPixelAt( int aPosX, int aPosY );

	/**
	 * Tests whether there is a pixel at a given position.
	 * 
	 * @param aPosX The x position for the pixel to be tested.
	 * @param aPosY The y position for the pixel to be tested.
	 * 
	 * @return True if we have a pixel at this position.
	 * 
	 * @throws IndexOutOfBoundsException in case the index is out of bounds.
	 */
	default boolean hasPixelAt( int aPosX, int aPosY ) {
		return getPixelAt( aPosX, aPosY ) != null;
	}

	/**
	 * Provides a mutator for a pixmap property.
	 *
	 * @param <PX> the generic type
	 */
	public interface PixmapMutator<PX> {

		/**
		 * Sets a pixel in the pixmap.
		 * 
		 * @param aPixel The pixel to be placed at the given position.
		 * @param aPosX The x position of the pixel.
		 * @param aPosY The y position of the pixel.
		 * 
		 * @throws IndexOutOfBoundsException in case the index is out of bounds.
		 */
		void setPixelAt( PX aPixel, int aPosX, int aPosY );

		/**
		 * Sets the pixels.
		 *
		 * @param aPixels the new pixels
		 */
		void setPixels( PX[][] aPixels );
	}

	/**
	 * Provides a builder method for a pixmap property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <PX> The type of the pixel being managed by the {@link Pixmap}.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PixmapBuilder<PX, B extends PixmapBuilder<PX, B>> extends PixmapProperty<PX> {

		/**
		 * Merges this {@link Pixmap} with the provided source {@link Pixmap}.
		 *
		 * @param aSourcePixmap The source {@link Pixmap} with which to merge.
		 * @param aSourcePosX The X position in the source {@link Pixmap} from
		 *        which to start merging.
		 * @param aSourcePosY The Y position in the source {@link Pixmap} from
		 *        which to start merging.
		 * @param isWrapHorizontically True in case merging is to be continued
		 *        on the left when reaching the {@link Pixmap} bounds on the
		 *        right.
		 * @param isWrapVertically True in case merging is to be continued on
		 *        the top when reaching the {@link Pixmap} bounds on the bottom.
		 * @param aDestionationPosX the destionation pos X
		 * @param aDestionationPosY the destionation pos Y
		 * @param aWidth The merge window's width.
		 * @param aHeight The merge window's height.
		 */
		default void mergeWith( B aSourcePixmap, int aSourcePosX, int aSourcePosY, boolean isWrapHorizontically, boolean isWrapVertically, int aDestionationPosX, int aDestionationPosY, int aWidth, int aHeight ) {
			mergeWith( aSourcePixmap, aSourcePosX, aSourcePosY, isWrapHorizontically, isWrapVertically, aDestionationPosX, aDestionationPosY, aWidth, aHeight, null );
		}

		/**
		 * Merges this {@link Pixmap} with the provided source {@link Pixmap}.
		 * 
		 * @param aSourcePixmap The source {@link Pixmap} with which to merge.
		 * @param aSourcePosX The X position in the source {@link Pixmap} from
		 *        which to start merging.
		 * @param aSourcePosY The Y position in the source {@link Pixmap} from
		 *        which to start merging.
		 * @param isWrapHorizontically True in case merging is to be continued
		 *        on the left when reaching the {@link Pixmap} bounds on the
		 *        right.
		 * @param isWrapVertically True in case merging is to be continued on
		 *        the top when reaching the {@link Pixmap} bounds on the bottom.
		 * @param aDestinationPosX The X position in the destination
		 *        {@link Pixmap} from which to start merging.
		 * @param aDestinationPosY The Y position in the destination
		 *        {@link Pixmap} from which to start merging.
		 * @param aWidth The merge window's width.
		 * @param aHeight The merge window's height.
		 * @param aTransparency The color indicating transparency, null when to
		 *        be ignored.
		 */
		default void mergeWith( B aSourcePixmap, int aSourcePosX, int aSourcePosY, boolean isWrapHorizontically, boolean isWrapVertically, int aDestinationPosX, int aDestinationPosY, int aWidth, int aHeight, Color aTransparency ) {
			for ( int x = 0; x < aWidth; ++x ) {
				int eSourceX = x + aSourcePosX;
				if ( isWrapHorizontically ) {
					while ( eSourceX >= aSourcePixmap.getWidth() ) {
						eSourceX -= aSourcePixmap.getWidth();
					}
				}

				if ( eSourceX >= aSourcePixmap.getWidth() || x + aDestinationPosX >= getWidth() ) {
					break;
				}

				for ( int y = 0; y < aHeight; ++y ) {
					int eSourceY = y + aSourcePosY;
					if ( isWrapVertically ) {
						while ( eSourceY >= aSourcePixmap.getHeight() ) {
							eSourceY -= aSourcePixmap.getHeight();
						}
					}

					if ( eSourceY >= aSourcePixmap.getHeight() || y + aDestinationPosY >= getHeight() ) {
						break;
					}

					final PX ePixel = aSourcePixmap.getPixelAt( eSourceX, eSourceY );

					Color theTransparency = null;
					if ( ePixel instanceof Color ) {
						theTransparency = (Color) ePixel;
					}
					if ( ePixel instanceof RgbPixel ) {
						theTransparency = ( (RgbPixel) ePixel ).toColor();
					}
					if ( aTransparency == null || theTransparency == null || !theTransparency.equals( aTransparency ) ) {
						setPixelAt( aSourcePixmap.getPixelAt( eSourceX, eSourceY ), x + aDestinationPosX, y + aDestinationPosY );
					}
				}
			}

		}

		/**
		 * Merge color arrays.
		 *
		 * @param aSourceColorArray the source color array
		 */
		default void mergeColorArrays( Pixmap<PX> aSourceColorArray ) {
			final int theWidth = getWidth();
			for ( int x = 0; x < theWidth && aSourceColorArray.getWidth() > x; ++x ) {
				for ( int y = 0; y < getHeight() && aSourceColorArray.getHeight() > y; ++y ) {
					setPixelAt( aSourceColorArray.getPixelAt( x, y ), x, y );
				}
			}

		}

		/**
		 * Sets the pixmap for the pixmap property.
		 *
		 * @param aPixel the pixel
		 * @param aPosX the pos X
		 * @param aPosY the pos Y
		 * 
		 * @return The builder for applying multiple build operations.
		 * 
		 * @throws IndexOutOfBoundsException in case the index is out of bounds.
		 */
		B withPixelAt( PX aPixel, int aPosX, int aPosY );

		/**
		 * With pixels.
		 *
		 * @param aPixels the pixels
		 * 
		 * @return the b
		 */
		B withPixels( PX[][] aPixels );
	}

	/**
	 * Provides a pixmap property.
	 * 
	 * @param <PX> The type of the pixel being managed by the {@link Pixmap}.
	 */
	public interface PixmapProperty<PX> extends Pixmap<PX>, PixmapMutator<PX> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given pixels (setter) as of
		 * {@link #setPixels(Object[][])} and returns the very same value
		 * (getter).
		 * 
		 * @param aPixels The pixels to set (via
		 *        {@link #setPixels(Object[][])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default PX[][] letPixels( PX[][] aPixels ) {
			setPixels( aPixels );
			return aPixels;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given pixels (setter) as of
		 * {@link #setPixelAt(Object, int, int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aPixel The pixel to set.
		 * @param aPosX The X position where to set the pixel.
		 * @param aPosY The Y position where to set the pixel.
		 * 
		 * @return Returns the pixel passed for it to be used in conclusive
		 *         processing steps.
		 */
		default PX letRgbAt( PX aPixel, int aPosX, int aPosY ) {
			setPixelAt( aPixel, aPosX, aPosY );
			return aPixel;
		}
	}
}
