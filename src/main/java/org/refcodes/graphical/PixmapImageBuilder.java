// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.refcodes.graphical.Dimension.DimensionBuilder;
import org.refcodes.graphical.Dimension.DimensionProperty;

/**
 * The Interface PixmapImageBuilder.
 *
 * @param <PX> the generic type
 */
public interface PixmapImageBuilder<PX> extends DimensionProperty, DimensionBuilder<PixmapImageBuilder<PX>> {

	/**
	 * To pixmap.
	 *
	 * @return the pixmap
	 * 
	 * @throws IOException thrown in case of IO related problems.
	 */
	Pixmap<PX> toPixmap() throws IOException;

	/**
	 * Sets the {@link InputStream} for retrieving the image data for the image
	 * property.
	 * 
	 * @param aImageStream The image which's {@link InputStream} is to be stored
	 *        by the image {@link InputStream} property.
	 */
	void setImageInputStream( InputStream aImageStream );

	/**
	 * Sets the input stream for retrieving the image data for the image
	 * property.
	 *
	 * @param aImageStream The image which's {@link InputStream} is to be stored
	 *        by the image {@link InputStream} property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	default PixmapImageBuilder<PX> withImageInputStream( InputStream aImageStream ) {
		setImageInputStream( aImageStream );
		return this;
	}

	/**
	 * Sets the {@link URL} for retrieving the image data for the image
	 * property.
	 * 
	 * @param aImageUrl The image which's {@link URL} is to be stored by the
	 *        image {@link URL} property.
	 */
	void setImageURL( URL aImageUrl );

	/**
	 * Sets the {@link URL} for retrieving the image data for the image
	 * property.
	 *
	 * @param aUrl the URL
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	default PixmapImageBuilder<PX> withImageURL( URL aUrl ) {
		setImageURL( aUrl );
		return this;
	}

	/**
	 * Sets the URL for retrieving the image data for the image property.
	 * 
	 * @param aImageFile The image file which's {@link InputStream} is to be
	 *        stored by the image {@link InputStream} property.
	 * 
	 * @throws FileNotFoundException in case the file was not found.
	 */
	default void setImageFile( File aImageFile ) throws FileNotFoundException {
		setImageInputStream( new FileInputStream( aImageFile ) );
	}

	/**
	 * Sets the {@link File} for retrieving the image data for the image
	 * property.
	 *
	 * @param aImageFile The image file which's {@link InputStream} is to be
	 *        stored by the image {@link InputStream} property.
	 * 
	 * @return The builder for applying multiple build operations.
	 * 
	 * @throws FileNotFoundException in case the file was not found.
	 */
	default PixmapImageBuilder<PX> withImageFile( File aImageFile ) throws FileNotFoundException {
		setImageInputStream( new FileInputStream( aImageFile ) );
		return this;
	}
}
