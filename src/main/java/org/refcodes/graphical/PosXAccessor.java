// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a X position property.
 */
public interface PosXAccessor {

	/**
	 * Retrieves the X position from the X position property.
	 * 
	 * @return The X position stored by the X position property.
	 */
	int getPositionX();

	/**
	 * Provides a mutator for a X position property.
	 */
	public interface PosXMutator {

		/**
		 * Sets the X position for the X position property.
		 * 
		 * @param aPosX The X position to be stored by the X position property.
		 */
		void setPositionX( int aPosX );
	}

	/**
	 * Provides a builder method for a X position property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PosXBuilder<B extends PosXBuilder<B>> {

		/**
		 * Sets the X position for the X position property.
		 * 
		 * @param aPosX The X position to be stored by the X position property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withPositionX( int aPosX );
	}

	/**
	 * Provides a X position property.
	 */
	public interface PosXProperty extends PosXAccessor, PosXMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setPositionX(int)} and returns the very same value (getter).
		 * 
		 * @param aPosX The integer to set (via {@link #setPositionX(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letPositionX( int aPosX ) {
			setPositionX( aPosX );
			return aPosX;
		}
	}
}
