// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a Y position property.
 */
public interface PosYAccessor {

	/**
	 * Retrieves the Y position from the Y position property.
	 * 
	 * @return The Y position stored by the Y position property.
	 */
	int getPositionY();

	/**
	 * Provides a mutator for a Y position property.
	 */
	public interface PosYMutator {

		/**
		 * Sets the Y position for the Y position property.
		 * 
		 * @param aPosY The Y position to be stored by the Y position property.
		 */
		void setPositionY( int aPosY );
	}

	/**
	 * Provides a builder method for a Y position property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PosYBuilder<B extends PosYBuilder<B>> {

		/**
		 * Sets the Y position for the Y position property.
		 * 
		 * @param aPosY The Y position to be stored by the Y position property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withPositionY( int aPosY );
	}

	/**
	 * Provides a Y position property.
	 */
	public interface PosYProperty extends PosYAccessor, PosYMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setPositionY(int)} and returns the very same value (getter).
		 * 
		 * @param aPosY The integer to set (via {@link #setPositionY(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letPositionY( int aPosY ) {
			setPositionY( aPosY );
			return aPosY;
		}
	}
}
