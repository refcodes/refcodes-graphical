// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a Y position property.
 */
public interface Position extends PosXAccessor, PosYAccessor {

	/**
	 * Adds the given {@link Position} to this {@link Position} (does not modify
	 * this {@link Position}) and returns the resulting {@link Position}.
	 * 
	 * @param aPosition The position to be added.
	 * 
	 * @return The resulting position.
	 */
	default Position toAddTo( Position aPosition ) {
		return new PositionImpl( aPosition.getPositionX() + getPositionX(), aPosition.getPositionY() + getPositionY() );
	}

	/**
	 * Subtracts the given {@link Position} from this {@link Position} (does not
	 * modify this {@link Position}) and returns the resulting {@link Position}.
	 * 
	 * @param aPosition The position to be subtracted.
	 * 
	 * @return The resulting position.
	 */
	default Position toSubFrom( Position aPosition ) {
		return new PositionImpl( getPositionX() - aPosition.getPositionX(), getPositionY() - aPosition.getPositionY() );
	}

	/**
	 * The Interface PositionMutator.
	 */
	public interface PositionMutator extends PosXMutator, PosYMutator {

		/**
		 * Sets the position.
		 *
		 * @param aPosX the pos X
		 * @param aPosY the pos Y
		 */
		void setPosition( int aPosX, int aPosY );

		/**
		 * Sets the position.
		 *
		 * @param aPosition the new position
		 */
		void setPosition( Position aPosition );
	}

	/**
	 * The Interface PositionBuilder.
	 *
	 * @param <B> the generic type
	 */
	public interface PositionBuilder<B extends PositionBuilder<B>> extends PosXBuilder<B>, PosYBuilder<B>, PositionMutator {

		/**
		 * With position.
		 *
		 * @param aPosX the pos X
		 * @param aPosY the pos Y
		 * 
		 * @return the b
		 */
		B withPosition( int aPosX, int aPosY );

		/**
		 * With position.
		 *
		 * @param aPosition the position
		 * 
		 * @return the b
		 */
		B withPosition( Position aPosition );
	}

	/**
	 * The Interface PositionProperty.
	 */
	public interface PositionProperty extends Position, PositionMutator, PosXProperty, PosYProperty {

		/**
		 * Adds the given {@link Position} to this {@link Position} (does not
		 * modify this {@link Position}).
		 * 
		 * @param aPosition The position to be added.
		 */
		default void addTo( Position aPosition ) {
			setPosition( aPosition.getPositionX() + getPositionX(), aPosition.getPositionY() + getPositionY() );
		}

		/**
		 * Subtracts the given {@link Position} from this {@link Position} (does
		 * modify this {@link Position}).
		 * 
		 * @param aPosition The position to be subtracted.
		 */
		default void subFrom( Position aPosition ) {
			setPosition( getPositionX() - aPosition.getPositionX(), getPositionY() - aPosition.getPositionY() );
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Position}
		 * (setter) as of {@link #setPosition(Position)} and returns the very
		 * same value (getter).
		 * 
		 * @param aPosition The {@link Position} to set (via
		 *        {@link #setPosition(Position)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Position letPosition( Position aPosition ) {
			setPosition( aPosition );
			return aPosition;
		}

		/**
		 * This method stores and passes through the given arguments, which is
		 * very useful for builder APIs: Sets the given {@link Dimension}
		 * (setter) as of {@link #setPosition(int, int)} and returns the very
		 * same values encapsulated as {@link Position} instance.
		 * 
		 * @param aPosX The width {@link Dimension} to set (via
		 *        {@link #setPositionX(int)}).
		 * @param aPosY The height {@link Dimension} to set (via
		 *        {@link #setPositionY(int)}).
		 * 
		 * @return Returns the values passed encapsulated in a {@link Position}
		 *         object for it to be used in conclusive processing steps.
		 */
		default Position letPosition( int aPosX, int aPosY ) {
			setPosition( aPosX, aPosY );
			return new Position() {
				@Override
				public int getPositionX() {
					return aPosX;
				}

				@Override
				public int getPositionY() {
					return aPosY;
				}
			};
		}
	}

	/**
	 * Equals.
	 *
	 * @param aPositionA the position A
	 * @param aPositionB the position B
	 * 
	 * @return true, if successful
	 */
	static boolean equals( Position aPositionA, Position aPositionB ) {
		return aPositionA.getPositionX() == aPositionB.getPositionX() && aPositionA.getPositionY() == aPositionB.getPositionY();
	}
}
