// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

import org.refcodes.graphical.Position.PositionBuilder;
import org.refcodes.graphical.Position.PositionProperty;

/**
 * The Class PositionPropertyBuilder.
 */
public class PositionPropertyBuilder extends PositionImpl implements PositionProperty, PositionBuilder<PositionPropertyBuilder> {

	/**
	 * Instantiates a new position property builder impl.
	 */
	public PositionPropertyBuilder() {}

	/**
	 * Instantiates a new position property builder impl.
	 *
	 * @param aPosX the pos X
	 * @param aPosY the pos Y
	 */
	public PositionPropertyBuilder( int aPosX, int aPosY ) {
		super( aPosX, aPosY );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPositionX( int aPosX ) {
		_posX = aPosX;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PositionPropertyBuilder withPositionX( int aPosX ) {
		_posX = aPosX;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPositionY( int aPosY ) {
		_posY = aPosY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PositionPropertyBuilder withPositionY( int aPosY ) {
		_posY = aPosY;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PositionPropertyBuilder withPosition( int aPosX, int aPosY ) {
		setPosition( aPosX, aPosY );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PositionPropertyBuilder withPosition( Position aPosition ) {
		setPosition( aPosition );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPosition( int aPosX, int aPosY ) {
		_posX = aPosX;
		_posY = aPosY;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPosition( Position aPosition ) {
		_posX = aPosition.getPositionX();
		_posY = aPosition.getPositionY();
	}
}