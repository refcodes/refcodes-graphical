// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * The {@link Raster} defines properties describing a matrix or a checkerboard.
 */
public interface Raster extends FieldDimension, GridDimension, GridModeAccessor {

	public interface RasterAccessor extends Raster {}

	/**
	 * The {@link RasterMutator} adds mutator functionality to a {@link Raster}.
	 */
	public interface RasterMutator extends FieldDimensionMutator, GridDimensionMutator, GridModeMutator {}

	/**
	 * The {@link RasterBuilder} adds builder functionality to a {@link Raster}.
	 *
	 * @param <B> the generic type ot the builder.
	 */
	public interface RasterBuilder<B extends RasterBuilder<B>> extends FieldDimensionBuilder<B>, GridDimensionBuilder<B>, GridModeBuilder<B> {}

	/**
	 * The {@link RasterProperty} type combines a {@link RasterAccessor} with a
	 * {@link RasterMutator}.
	 */
	public interface RasterProperty extends RasterAccessor, RasterMutator, FieldDimensionProperty, GridDimensionProperty, GridModeProperty {}

}
