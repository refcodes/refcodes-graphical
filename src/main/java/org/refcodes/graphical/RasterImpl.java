// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * The Class RasterImpl.
 *
 * @author steiner
 */
public class RasterImpl implements Raster {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected int _gridWidth = -1;

	protected int _gridHeight = -1;

	protected int _fieldHeight = -1;

	protected int _fieldWidth = -1;

	protected int _fieldGap = 0;

	protected GridMode _gridMode = GridMode.CLOSED;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new raster impl.
	 */
	protected RasterImpl() {}

	/**
	 * Instantiates a new raster impl.
	 *
	 * @param aGridWidth the grid width
	 * @param aGridHeight the grid height
	 * @param aWidth the width
	 * @param aHeight the height
	 * @param aGap the gap
	 * @param aGridMode the grid mode
	 */
	public RasterImpl( int aGridWidth, int aGridHeight, int aWidth, int aHeight, int aGap, GridMode aGridMode ) {
		_gridWidth = aGridWidth;
		_gridHeight = aGridHeight;
		_fieldWidth = aWidth;
		_fieldHeight = aHeight;
		_fieldGap = aGap;
		_gridMode = aGridMode;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getGridWidth() {
		return _gridWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public GridMode getGridMode() {
		return _gridMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFieldGap() {
		return _fieldGap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFieldWidth() {
		return _fieldWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFieldHeight() {
		return _fieldHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getGridHeight() {
		return _gridHeight;
	}
}
