// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

import org.refcodes.graphical.Raster.RasterBuilder;
import org.refcodes.graphical.Raster.RasterProperty;

/**
 * The Class RasterPropertyBuilder.
 */
public class RasterPropertyBuilder extends RasterImpl implements RasterProperty, RasterBuilder<RasterPropertyBuilder> {

	/**
	 * Instantiates a new raster property builder impl.
	 */
	public RasterPropertyBuilder() {}

	/**
	 * Instantiates a new raster property builder impl.
	 *
	 * @param aGridWidth the grid width
	 * @param aGridHeight the grid height
	 * @param aWidth the width
	 * @param aHeight the height
	 * @param aGap the gap
	 * @param aGridMode the grid mode
	 */
	public RasterPropertyBuilder( int aGridWidth, int aGridHeight, int aWidth, int aHeight, int aGap, GridMode aGridMode ) {
		super( aGridWidth, aGridHeight, aWidth, aHeight, aGap, aGridMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setGridDimension( int aWidth, int aHeight ) {
		_gridWidth = aWidth;
		_gridHeight = aHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setGridDimension( GridDimension aDimension ) {
		setGridDimension( aDimension.getGridWidth(), aDimension.getGridHeight() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setGridDimension( Dimension aDimension ) {
		setGridDimension( aDimension.getWidth(), aDimension.getHeight() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setGridWidth( int aWidth ) {
		setGridDimension( aWidth, _gridHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setGridHeight( int aHeight ) {
		setGridDimension( _gridWidth, aHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RasterPropertyBuilder withGridDimension( int aGridWidth, int aGridHeight ) {
		setGridDimension( aGridWidth, aGridHeight );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RasterPropertyBuilder withGridDimension( GridDimension aDimension ) {
		setGridDimension( aDimension );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RasterPropertyBuilder withGridDimension( Dimension aDimension ) {
		setGridDimension( aDimension );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RasterPropertyBuilder withGridWidth( int aWidth ) {
		setGridWidth( aWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RasterPropertyBuilder withGridHeight( int aHeight ) {
		setGridHeight( aHeight );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setGridMode( GridMode aGridMode ) {
		_gridMode = aGridMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RasterPropertyBuilder withGridMode( GridMode aGridMode ) {
		setGridMode( aGridMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RasterPropertyBuilder withFieldWidth( int aWidth ) {
		setFieldWidth( aWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RasterPropertyBuilder withFieldHeight( int aHeight ) {
		setFieldHeight( aHeight );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RasterPropertyBuilder withFieldDimension( int aWidth, int aHeight ) {
		setFieldDimension( aWidth, aHeight );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RasterPropertyBuilder withFieldDimension( int aWidth, int aHeight, int aGap ) {
		setFieldDimension( aWidth, aHeight, aGap );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RasterPropertyBuilder withFieldDimension( FieldDimension aField ) {
		setFieldDimension( aField );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RasterPropertyBuilder withFieldDimension( Dimension aDimension ) {
		setFieldDimension( aDimension );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldGap( int aGap ) {
		_fieldGap = aGap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RasterPropertyBuilder withFieldGap( int aGap ) {
		setFieldGap( aGap );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldDimension( int aWidth, int aHeight ) {
		_fieldWidth = aWidth;
		_fieldHeight = aHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldDimension( int aWidth, int aHeight, int aGap ) {
		_fieldWidth = aWidth;
		_fieldHeight = aHeight;
		_fieldGap = aGap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldDimension( FieldDimension aDimension ) {
		setFieldDimension( aDimension.getFieldWidth(), aDimension.getFieldHeight(), aDimension.getFieldGap() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldDimension( Dimension aDimension ) {
		setFieldDimension( aDimension.getWidth(), aDimension.getHeight() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldWidth( int aWidth ) {
		setFieldDimension( aWidth, _fieldHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldHeight( int aHeight ) {
		setFieldDimension( _fieldWidth, aHeight );
	}
}