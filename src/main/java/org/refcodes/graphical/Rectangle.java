// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a rectangle property (dimension and height).
 */
public interface Rectangle extends Dimension, Position {

	/**
	 * Provides a mutator for a rectangle property (dimension and height).
	 */
	public interface RectangleMutator extends DimensionMutator, PositionMutator {

		/**
		 * Sets the rectangle.
		 *
		 * @param aPositionX The X position of the rectangle.
		 * @param aPositionY The Y position of the rectangle.
		 * @param aWidth The width of the rectangle.
		 * @param aHeight The height of the rectangle.
		 */
		void setRectangle( int aPositionX, int aPositionY, int aWidth, int aHeight );

		/**
		 * Sets the rectangle.
		 *
		 * @param aRectangle the new rectangle's metrics to set.
		 */
		void setRectangle( Rectangle aRectangle );
	}

	/**
	 * Provides a builder for a rectangle property (dimension and height).
	 *
	 * @param <B> the generic type
	 */
	public interface RectangleBuilder<B extends RectangleBuilder<B>> extends DimensionBuilder<B>, PositionBuilder<B> {

		/**
		 * With rectangle.
		 *
		 * @param aPositionX The X position of the rectangle.
		 * @param aPositionY The Y position of the rectangle.
		 * @param aWidth The width of the rectangle.
		 * @param aHeight The height of the rectangle.
		 * 
		 * @return The invoked instance as of the builder pattern.
		 */
		B withRectangle( int aPositionX, int aPositionY, int aWidth, int aHeight );

		/**
		 * With rectangle.
		 *
		 * @param aRectangle The rectangle's metrics to set.
		 * 
		 * @return The invoked instance as of the builder pattern.
		 */
		B withRectangle( Rectangle aRectangle );
	}

	/**
	 * Provides a property (getter / setter) for a rectangle property (dimension
	 * and height).
	 */
	public interface RectangleProperty extends Rectangle, RectangleMutator, DimensionProperty, PositionProperty {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Rectangle}
		 * (setter) as of {@link #setRectangle(Rectangle)} and returns the very
		 * same value (getter).
		 * 
		 * @param aRectangle The new rectangle's metrics to set (via
		 *        {@link #setRectangle(Rectangle)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Rectangle letRectangle( Rectangle aRectangle ) {
			setRectangle( aRectangle );
			return aRectangle;
		}

		/**
		 * This method stores and passes through the given arguments, which is
		 * very useful for builder APIs: Sets the given {@link Rectangle}
		 * (setter) as of {@link #setRectangle(int, int, int, int)} and returns
		 * the very same value encapsulated as {@link Rectangle} instance.
		 * 
		 * @param aPositionX The X position of the rectangle.
		 * @param aPositionY The Y position of the rectangle.
		 * @param aWidth The width of the rectangle.
		 * @param aHeight The height of the rectangle.
		 * 
		 * @return Returns the values passed encapsulated in a {@link Rectangle}
		 *         object for it to be used in conclusive processing steps.
		 */
		default Rectangle letRectangle( int aPositionX, int aPositionY, int aWidth, int aHeight ) {
			setRectangle( aPositionX, aPositionY, aWidth, aHeight );
			return new RectangleImpl( aPositionX, aPositionY, aWidth, aHeight );
		}
	}

	/**
	 * Equals.
	 *
	 * @param aRectangleA the rectangle A
	 * @param aRectangleB the rectangle B
	 * 
	 * @return true, if successful
	 */
	static boolean equals( Rectangle aRectangleA, Rectangle aRectangleB ) {
		return aRectangleA.getPositionX() == aRectangleB.getPositionX() && aRectangleA.getPositionY() == aRectangleB.getPositionY() && aRectangleA.getWidth() == aRectangleB.getWidth() && aRectangleA.getHeight() == aRectangleB.getHeight();
	}
}
