// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * The class {@link RectangleImpl} implements the {@link Rectangle} interface.
 */
public class RectangleImpl implements Rectangle {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final int _width;
	private final int _height;
	private final int _posX;
	private final int _posY;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link Rectangle} with the given metrics.
	 * 
	 * @param aPositionX The X position of the rectangle.
	 * @param aPositionY The Y position of the rectangle.
	 * @param aWidth The width of the rectangle.
	 * @param aHeight The height of the rectangle.
	 */
	public RectangleImpl( int aPositionX, int aPositionY, int aWidth, int aHeight ) {
		_posX = aPositionX;
		_posY = aPositionY;
		_width = aWidth;
		_height = aHeight;
	}

	/**
	 * Constructs a {@link Rectangle} with the given metrics.
	 * 
	 * @param aPosition The position of the rectangle.
	 * @param aDimension The dimension of the rectangle.
	 */
	public RectangleImpl( Position aPosition, Dimension aDimension ) {
		_posX = aPosition.getPositionX();
		_posY = aPosition.getPositionY();
		_width = aDimension.getWidth();
		_height = aDimension.getHeight();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getWidth() {
		return _width;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getHeight() {
		return _height;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPositionX() {
		return _posX;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPositionY() {
		return _posY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + _height;
		result = prime * result + _posX;
		result = prime * result + _posY;
		result = prime * result + _width;
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final RectangleImpl other = (RectangleImpl) obj;
		if ( _height != other._height ) {
			return false;
		}
		if ( _posX != other._posX ) {
			return false;
		}
		if ( _posY != other._posY ) {
			return false;
		}
		return _width != other._width ? false : true;
	}
}
