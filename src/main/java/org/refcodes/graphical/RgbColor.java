// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

import java.awt.Color;

/**
 * The Interface RgbPixel.
 */
public interface RgbColor {

	/**
	 * Gets the alpha portion.
	 *
	 * @return the alpha portion
	 */
	int getAlpha();

	/**
	 * Gets the red portion.
	 *
	 * @return the red portion
	 */
	int getRed();

	/**
	 * Gets the green portion.
	 *
	 * @return the green portion
	 */
	int getGreen();

	/**
	 * Gets the blue portion.
	 *
	 * @return the blue portion
	 */
	int getBlue();

	/**
	 * To rgb value.
	 *
	 * @return the int
	 */
	int toRgbValue();

	/**
	 * Returns the {@link Color} representation of this RGB pixel.
	 * 
	 * @return The according AWT's {@link Color} of this {@link RgbColor}.
	 */
	default Color toColor() {
		return new Color( getRed(), getGreen(), getBlue(), getAlpha() );
	}

	/**
	 * Sets the alpha portion.
	 *
	 * @param aAlphaPortion the new alpha portion
	 */
	void setAlphaPortion( int aAlphaPortion );

	/**
	 * Sets the red portion.
	 *
	 * @param aRedPortion the new red portion
	 */
	void setRedPortion( int aRedPortion );

	/**
	 * Sets the green portion.
	 *
	 * @param aGreenPortion the new green portion
	 */
	void setGreenPortion( int aGreenPortion );

	/**
	 * Sets the blue portion.
	 *
	 * @param aBluePortion the new blue portion
	 */
	void setBluePortion( int aBluePortion );

	/**
	 * Sets the rgb value.
	 *
	 * @param aRgbValue the new rgb value
	 */
	void setRgbValue( int aRgbValue );

	/**
	 * With alpha portion.
	 *
	 * @param aAlphaPortion the alpha portion
	 * 
	 * @return the rgb pixel
	 */
	default RgbColor withAlphaPortion( int aAlphaPortion ) {
		setAlphaPortion( aAlphaPortion );
		return this;
	}

	/**
	 * With red portion.
	 *
	 * @param aRedPortion the red portion
	 * 
	 * @return the rgb pixel
	 */
	default RgbColor withRedPortion( int aRedPortion ) {
		setRedPortion( aRedPortion );
		return this;
	}

	/**
	 * With green portion.
	 *
	 * @param aGreenPortion the green portion
	 * 
	 * @return the rgb pixel
	 */
	default RgbColor withGreenPortion( int aGreenPortion ) {
		setGreenPortion( aGreenPortion );
		return this;
	}

	/**
	 * With blue portion.
	 *
	 * @param aBluePortion the blue portion
	 * 
	 * @return the rgb pixel
	 */
	default RgbColor withBluePortion( int aBluePortion ) {
		setBluePortion( aBluePortion );
		return this;
	}

	/**
	 * With rgb value.
	 *
	 * @param aRgbValue the rgb value
	 * 
	 * @return the rgb pixel
	 */
	default RgbColor withRgbValue( int aRgbValue ) {
		setRgbValue( aRgbValue );
		return this;
	}
}
