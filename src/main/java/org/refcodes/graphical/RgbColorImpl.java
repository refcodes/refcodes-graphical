// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

import java.awt.Color;

/**
 * The Class RgbColorImpl.
 */
public class RgbColorImpl implements RgbColor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _alphaPortion = 0;
	private int _redPortion = 0;
	private int _greenPortion = 0;
	private int _bluePortion = 0;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new rgb pixel impl.
	 */
	public RgbColorImpl() {};

	/**
	 * Instantiates a new rgb pixel impl.
	 *
	 * @param aAlphaPortion the alpha portion
	 * @param aRedPortion the red portion
	 * @param aGreenPortion the green portion
	 * @param aBluePortion the blue portion
	 */
	public RgbColorImpl( int aAlphaPortion, int aRedPortion, int aGreenPortion, int aBluePortion ) {
		_alphaPortion = aAlphaPortion;
		_redPortion = aRedPortion;
		_greenPortion = aGreenPortion;
		_bluePortion = aBluePortion;
	}

	/**
	 * Instantiates a new rgb pixel impl.
	 *
	 * @param aRgbValue the rgb value
	 */
	public RgbColorImpl( int aRgbValue ) {
		setRgbValue( aRgbValue );
	}

	/**
	 * Instantiates a new rgb pixel impl.
	 *
	 * @param aAwtColor the rgb value
	 */
	public RgbColorImpl( Color aAwtColor ) {
		this( aAwtColor.getAlpha(), aAwtColor.getRed(), aAwtColor.getGreen(), aAwtColor.getBlue() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getAlpha() {
		return _alphaPortion;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getRed() {
		return _redPortion;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getGreen() {
		return _greenPortion;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getBlue() {
		return _bluePortion;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int toRgbValue() {
		return GraphicalUtility.toRgb( _alphaPortion, _redPortion, _greenPortion, _bluePortion );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRgbValue( int aRgbValue ) {
		_alphaPortion = GraphicalUtility.toAlpha( aRgbValue );
		_redPortion = GraphicalUtility.toRed( aRgbValue );
		_greenPortion = GraphicalUtility.toGreen( aRgbValue );
		_bluePortion = GraphicalUtility.toBlue( aRgbValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setAlphaPortion( int aAlphaPortion ) {
		_alphaPortion = aAlphaPortion;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRedPortion( int aRedPortion ) {
		_redPortion = aRedPortion;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setGreenPortion( int aGreenPortion ) {
		_greenPortion = aGreenPortion;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBluePortion( int aBluePortion ) {
		_bluePortion = aBluePortion;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + _alphaPortion;
		result = prime * result + _bluePortion;
		result = prime * result + _greenPortion;
		result = prime * result + _redPortion;
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final RgbColorImpl other = (RgbColorImpl) obj;
		if ( _alphaPortion != other._alphaPortion ) {
			return false;
		}
		if ( _bluePortion != other._bluePortion ) {
			return false;
		}
		if ( _greenPortion != other._greenPortion ) {
			return false;
		}
		if ( _redPortion != other._redPortion ) {
			return false;
		}
		return true;
	}
}
