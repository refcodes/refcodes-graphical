// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

import java.awt.Color;

/**
 * The Interface RgbPixmap.
 *
 * @author steiner
 */
public interface RgbPixmap extends Pixmap<RgbPixel> {

	/**
	 * Returns the AWT's {@link Color} representation of the pixel at the
	 * according position.
	 * 
	 * @param aPosX The x position of the pixel.
	 * @param aPosY The y position of the pixel.
	 * 
	 * @return The according {@link Color}.
	 */
	default int getRgbAt( int aPosX, int aPosY ) {
		final RgbPixel thePixel = getPixelAt( aPosX, aPosY );
		return GraphicalUtility.toRgb( thePixel.getAlpha(), thePixel.getRed(), thePixel.getGreen(), thePixel.getBlue() );
	}

	/**
	 * Returns the AWT's {@link Color} representation of the pixel at the
	 * according position.
	 * 
	 * @param aPosX The x position of the pixel.
	 * @param aPosY The y position of the pixel.
	 * 
	 * @return The according {@link Color}.
	 */
	default Color getColorAt( int aPosX, int aPosY ) {
		final RgbPixel thePixel = getPixelAt( aPosX, aPosY );
		return thePixel != null ? thePixel.toColor() : null;
	}

	/**
	 * The Interface RgbPixmapMutator.
	 */
	public interface RgbPixmapMutator extends PixmapMutator<RgbPixel> {

		/**
		 * Sets an integer value interpreted as pixel in the pixmap.
		 * 
		 * @param aRgbPixel The pixel to be placed at the given position.
		 * @param aPosX The x position of the pixel.
		 * @param aPosY The y position of the pixel.
		 * 
		 * @throws IndexOutOfBoundsException in case the index is out of bounds.
		 */
		default void setRgbAt( int aRgbPixel, int aPosX, int aPosY ) {
			setPixelAt( new RgbPixelImpl( aRgbPixel ), aPosX, aPosY );
		}

		/**
		 * Sets an integer value interpreted as pixel in the pixmap.
		 * 
		 * @param aPixel The pixel to be placed at the given position.
		 * @param aPosX The x position of the pixel.
		 * @param aPosY The y position of the pixel.
		 * 
		 * @throws IndexOutOfBoundsException in case the index is out of bounds.
		 */
		default void setColorAt( Color aPixel, int aPosX, int aPosY ) {
			setRgbAt( aPixel.getRGB(), aPosX, aPosY );
		}
	}

	/**
	 * The Interface RgbPixmapProperty.
	 */
	public interface RgbPixmapProperty extends RgbPixmap, RgbPixmapMutator, PixmapProperty<RgbPixel> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given pixels (setter) as of
		 * {@link #setRgbAt(int, int, int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aRgbPixel The pixel to set.
		 * @param aPosX The X position where to set the pixel.
		 * @param aPosY The Y position where to set the pixel.
		 * 
		 * @return Returns the pixel passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letRgbAt( int aRgbPixel, int aPosX, int aPosY ) {
			setRgbAt( aRgbPixel, aPosX, aPosY );
			return aRgbPixel;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given pixels (setter) as of
		 * {@link #setRgbAt(int, int, int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aPixel The pixel to set.
		 * @param aPosX The X position where to set the pixel.
		 * @param aPosY The Y position where to set the pixel.
		 * 
		 * @return Returns the pixel passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Color letColorAt( Color aPixel, int aPosX, int aPosY ) {
			setColorAt( aPixel, aPosX, aPosY );
			return aPixel;
		}
	}

	/**
	 * The Interface RgbPixmapBuilder.
	 */
	public interface RgbPixmapBuilder extends PixmapBuilder<RgbPixel, RgbPixmapBuilder>, RgbPixmapProperty {

		/**
		 * Sets an integer value interpreted as pixel in the pixmap.
		 *
		 * @param aRgbPixel The pixel to be placed at the given position.
		 * @param aPosX The x position of the pixel.
		 * @param aPosY The y position of the pixel.
		 * 
		 * @return the RgbPixmapBuilder
		 * 
		 * @throws IndexOutOfBoundsException in case the index is out of bounds.
		 */
		default RgbPixmapBuilder withRgbAt( int aRgbPixel, int aPosX, int aPosY ) {
			setRgbAt( aRgbPixel, aPosX, aPosY );
			return this;
		}

		/**
		 * Sets an integer value interpreted as pixel in the pixmap.
		 *
		 * @param aPixel The pixel to be placed at the given position.
		 * @param aPosX The x position of the pixel.
		 * @param aPosY The y position of the pixel.
		 * 
		 * @return the RgbPixmapBuilder
		 * 
		 * @throws IndexOutOfBoundsException in case the index is out of bounds.
		 */
		default RgbPixmapBuilder withColorlAt( Color aPixel, int aPosX, int aPosY ) {
			setColorAt( aPixel, aPosX, aPosY );
			return this;
		}
	}
}
