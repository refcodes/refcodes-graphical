// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.refcodes.graphical.RgbPixmap.RgbPixmapBuilder;

/**
 * The Class RgbPixmapBuilderImpl.
 */
public class RgbPixmapBuilderImpl extends RgbPixmapImpl implements RgbPixmapBuilder {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public RgbPixmapBuilderImpl( BufferedImage aImage, int aWidth, int aHeight ) {
		super( aImage, aWidth, aHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	public RgbPixmapBuilderImpl( BufferedImage aImage ) {
		super( aImage );
	}

	/**
	 * {@inheritDoc}
	 */
	public RgbPixmapBuilderImpl( File aImageFile, int aWidth, int aHeight ) throws IOException {
		super( aImageFile, aWidth, aHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	public RgbPixmapBuilderImpl( File aImageFile ) throws IOException {
		super( aImageFile );
	}

	/**
	 * {@inheritDoc}
	 */
	public RgbPixmapBuilderImpl( InputStream aImageInputStream, int aWidth, int aHeight ) throws IOException {
		super( aImageInputStream, aWidth, aHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	public RgbPixmapBuilderImpl( InputStream aImageInputStream ) throws IOException {
		super( aImageInputStream );
	}

	/**
	 * {@inheritDoc}
	 */
	public RgbPixmapBuilderImpl( URL aImageUrl, int aWidth, int aHeight ) throws IOException {
		super( aImageUrl, aWidth, aHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	public RgbPixmapBuilderImpl( URL aImageUrl ) throws IOException {
		super( aImageUrl );
	}

	/**
	 * {@inheritDoc}
	 */
	public RgbPixmapBuilderImpl() {}

	/**
	 * {@inheritDoc}
	 */
	public RgbPixmapBuilderImpl( int aWidth, int aHeight ) {
		super( aWidth, aHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	public RgbPixmapBuilderImpl( RgbPixel[][] aPixels ) {
		super( aPixels );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * {@inheritDoc}
	 */
	@Override
	public RgbPixmapBuilderImpl withPixels( RgbPixel[][] aPixels ) {
		setPixels( aPixels );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPixelAt( RgbPixel aPixel, int aPosX, int aPosY ) {
		_pixels[aPosX][aPosY] = aPixel;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPixels( RgbPixel[][] aPixels ) {
		_pixels = aPixels;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RgbPixmapBuilderImpl withPixelAt( RgbPixel aPixel, int aPosX, int aPosY ) {
		setPixelAt( aPixel, aPosX, aPosY );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RgbPixmapBuilderImpl withRgbAt( int aPixel, int aPosX, int aPosY ) {
		setRgbAt( aPixel, aPosX, aPosY );
		return this;
	}
}