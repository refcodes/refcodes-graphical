// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * The Class RgbPixmapImageBuilderImpl.
 */
public class RgbPixmapImageBuilder implements PixmapImageBuilder<RgbPixel> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private InputStream _imageStream = null;
	private URL _imageUrl = null;
	private int _width = -1;
	private int _height = -1;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getWidth() {
		return _width;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getHeight() {
		return _height;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDimension( int aWidth, int aHeight ) {
		_width = aWidth;
		_height = aHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDimension( Dimension aDimension ) {
		_width = aDimension.getWidth();
		_height = aDimension.getHeight();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setWidth( int aWidth ) {
		_width = aWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeight( int aHeight ) {
		_height = aHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RgbPixmapImageBuilder withDimension( int aWidth, int aHeight ) {
		setDimension( aWidth, aHeight );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RgbPixmapImageBuilder withDimension( Dimension aDimension ) {
		setDimension( aDimension );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RgbPixmapImageBuilder withWidth( int aWidth ) {
		setWidth( aWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RgbPixmapImageBuilder withHeight( int aHeight ) {
		setHeight( aHeight );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setImageInputStream( InputStream aImageStream ) {
		_imageStream = aImageStream;
		_imageUrl = null;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setImageURL( URL aImageUrl ) {
		_imageStream = null;
		_imageUrl = aImageUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RgbPixmap toPixmap() throws IOException {
		if ( _imageUrl != null ) {
			return new RgbPixmapImpl( _imageUrl, _width, _height );
		}
		if ( _imageStream != null ) {
			return new RgbPixmapImpl( _imageStream, _width, _height );
		}
		throw new IllegalStateException( "Either an image URL or an image Input-Stream (File) must be set to produce a pixmap!" );
	}

	/**
	 * To pixmap.
	 *
	 * @param aImageStream the image stream
	 * 
	 * @return the rgb pixmap
	 * 
	 * @throws IOException thrown in case there were IO problems.
	 */
	public RgbPixmap toPixmap( InputStream aImageStream ) throws IOException {
		return new RgbPixmapImpl( aImageStream, _width, _height );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RgbPixmapImageBuilder withImageInputStream( InputStream aImageStream ) {
		setImageInputStream( aImageStream );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RgbPixmapImageBuilder withImageFile( File aImageFile ) throws FileNotFoundException {
		setImageFile( aImageFile );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RgbPixmapImageBuilder withImageURL( URL aImageUrl ) {
		setImageURL( aImageUrl );
		return this;
	}

	/**
	 * To pixmap.
	 *
	 * @param aImageFile the image file
	 * 
	 * @return the rgb pixmap
	 * 
	 * @throws IOException thrown in ase there were IO problems.
	 */
	public RgbPixmap toPixmap( File aImageFile ) throws IOException {
		return new RgbPixmapImpl( aImageFile );
	}
}
