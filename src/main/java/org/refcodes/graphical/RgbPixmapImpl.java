// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.imageio.ImageIO;

/**
 * The Class RgbPixmapImpl.
 *
 * @author steiner
 */
public class RgbPixmapImpl implements RgbPixmap {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected RgbPixel[][] _pixels = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new rgb pixmap impl.
	 */
	public RgbPixmapImpl() {}

	/**
	 * Instantiates a new rgb pixmap impl.
	 *
	 * @param aPixels the pixels
	 */
	public RgbPixmapImpl( RgbPixel[][] aPixels ) {
		_pixels = aPixels;
	}

	/**
	 * Instantiates a new rgb pixmap impl.
	 *
	 * @param aWidth the width
	 * @param aHeight the height
	 */
	public RgbPixmapImpl( int aWidth, int aHeight ) {
		_pixels = new RgbPixel[aWidth][aHeight];
	}

	/**
	 * Constructs a {@link RgbPixmap} from the given {@link BufferedImage}.
	 *
	 * @param aImage the image from which to get the pixmap.
	 */
	public RgbPixmapImpl( BufferedImage aImage ) {
		this( aImage, -1, -1 );
	}

	/**
	 * Constructs a {@link RgbPixmap} from the given image's {@link URL}.
	 *
	 * @param aImageUrl the image URL from which to get the pixmap.
	 * 
	 * @throws IOException thrown in case reading the image encountered
	 *         problems.
	 */
	public RgbPixmapImpl( URL aImageUrl ) throws IOException {
		this( aImageUrl, -1, -1 );
	}

	/**
	 * Constructs a {@link RgbPixmap} from the given {@link File}.
	 *
	 * @param aImageFile the image {@link File} from which to get the pixmap.
	 * 
	 * @throws IOException thrown in case reading the image encountered
	 *         problems.
	 */
	public RgbPixmapImpl( File aImageFile ) throws IOException {
		this( aImageFile, -1, -1 );
	}

	/**
	 * Constructs a {@link RgbPixmap} from the given image {@link InputStream}.
	 *
	 * @param aImageInputStream the image {@link InputStream} from which to get
	 *        the pixmap.
	 * 
	 * @throws IOException thrown in case reading the image encountered
	 *         problems.
	 */
	public RgbPixmapImpl( InputStream aImageInputStream ) throws IOException {
		this( aImageInputStream, -1, -1 );
	}

	/**
	 * Constructs a {@link RgbPixmap} from the given image's {@link URL} and the
	 * given dimension for the resulting pixmap.
	 *
	 * @param aImageUrl the image URL from which to get the pixmap.
	 * @param aWidth The width to for the resulting pixmap, -1 if the image's
	 *        width is to be used.
	 * @param aHeight the height for the resulting pixmap, -1 if the image's
	 *        height is to be used.
	 * 
	 * @throws IOException thrown in case reading the image encountered
	 *         problems.
	 */
	public RgbPixmapImpl( URL aImageUrl, int aWidth, int aHeight ) throws IOException {
		this( ImageIO.read( aImageUrl ), aWidth, aHeight );
	}

	/**
	 * Constructs a {@link RgbPixmap} from the given {@link File} and the given
	 * dimension for the resulting pixmap.
	 *
	 * @param aImageFile the image {@link File} from which to get the pixmap.
	 * @param aWidth The width to for the resulting pixmap, -1 if the image's
	 *        width is to be used.
	 * @param aHeight the height for the resulting pixmap, -1 if the image's
	 *        height is to be used.
	 * 
	 * @throws IOException thrown in case reading the image encountered
	 *         problems.
	 */
	public RgbPixmapImpl( File aImageFile, int aWidth, int aHeight ) throws IOException {
		this( new FileInputStream( aImageFile ), aWidth, aHeight );
	}

	/**
	 * Constructs a {@link RgbPixmap} from the given image {@link InputStream}
	 * and the given dimension for the resulting pixmap.
	 *
	 * @param aImageInputStream the image {@link InputStream} from which to get
	 *        the pixmap.
	 * @param aWidth The width to for the resulting pixmap, -1 if the image's
	 *        width is to be used.
	 * @param aHeight the height for the resulting pixmap, -1 if the image's
	 *        height is to be used.
	 * 
	 * @throws IOException thrown in case reading the image encountered
	 *         problems.
	 */
	public RgbPixmapImpl( InputStream aImageInputStream, int aWidth, int aHeight ) throws IOException {
		this( ImageIO.read( aImageInputStream ), aWidth, aHeight );
	}

	/**
	 * Constructs a {@link RgbPixmap} from the given {@link BufferedImage} and
	 * the given dimension for the resulting pixmap.
	 *
	 * @param aImage the image from which to get the pixmap.
	 * @param aWidth The width to for the resulting pixmap, -1 if the image's
	 *        width is to be used.
	 * @param aHeight the height for the resulting pixmap, -1 if the image's
	 *        height is to be used.
	 */
	public RgbPixmapImpl( BufferedImage aImage, int aWidth, int aHeight ) {
		int theToWidth = aWidth;
		int theToHeight = aHeight;
		final int theFromWidth = aImage.getWidth();
		final int theFromHeight = aImage.getHeight();
		if ( theToWidth == -1 && theToHeight != -1 ) {
			theToWidth = (int) ( ( (float) theFromWidth ) / ( (float) theFromHeight ) * ( (float) theToHeight ) );
		}
		if ( theToWidth != -1 && theToHeight == -1 ) {
			theToHeight = (int) ( ( (float) theToWidth ) / ( (float) theFromWidth ) * ( (float) theFromHeight ) );
		}
		if ( theToWidth == -1 && theToHeight == -1 ) {
			theToWidth = theFromWidth;
			theToHeight = theFromHeight;
		}
		BufferedImage theScaled = new BufferedImage( theToWidth, theToHeight, aImage.getType() );
		final AffineTransform theTransform = new AffineTransform();
		theTransform.scale( theToWidth, theToHeight );
		final AffineTransformOp theScaleOperation = new AffineTransformOp( theTransform, AffineTransformOp.TYPE_BILINEAR );
		theScaled = theScaleOperation.filter( aImage, theScaled );
		_pixels = new RgbPixel[theToWidth][theToHeight];
		for ( int x = 0; x < theToWidth; x++ ) {
			for ( int y = 0; y < theToHeight; y++ ) {
				_pixels[x][y] = new RgbPixelImpl( aImage.getRGB( x, y ) );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RgbPixel[][] getPixels() {
		return _pixels;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getWidth() {
		return _pixels.length;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getHeight() {
		return _pixels[0].length;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RgbPixel getPixelAt( int aPosX, int aPosY ) {
		return _pixels[aPosX][aPosY];
	}
}
