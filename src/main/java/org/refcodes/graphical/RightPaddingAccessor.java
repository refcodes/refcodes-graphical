// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a right padding property.
 */
public interface RightPaddingAccessor {

	/**
	 * Retrieves the right padding from the right padding property.
	 * 
	 * @return The right padding stored by the right padding property.
	 */
	int getRightPadding();

	/**
	 * Provides a mutator for a right padding property.
	 */
	public interface RightPaddingMutator {

		/**
		 * Sets the right padding for the right padding property.
		 * 
		 * @param aRightPadding The right padding to be stored by the right
		 *        padding property.
		 */
		void setRightPadding( int aRightPadding );
	}

	/**
	 * Provides a builder method for a right padding property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface RightPaddingBuilder<B extends RightPaddingBuilder<B>> {

		/**
		 * Sets the right padding for the right padding property.
		 * 
		 * @param aRightPadding The right padding to be stored by the right
		 *        padding property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withRightPadding( int aRightPadding );
	}

	/**
	 * Provides a right padding property.
	 */
	public interface RightPaddingProperty extends RightPaddingAccessor, RightPaddingMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setRightPadding(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aRightPadding The integer to set (via
		 *        {@link #setRightPadding(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letRightPadding( int aRightPadding ) {
			setRightPadding( aRightPadding );
			return aRightPadding;
		}
	}
}
