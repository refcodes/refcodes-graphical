/*
 * 
 */
package org.refcodes.graphical;

/**
 * The Enum ScaleMode.
 */
public enum ScaleMode {

	NONE, GRID, FIELDS

}
