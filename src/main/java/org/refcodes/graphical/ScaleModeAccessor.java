// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a scale mode property.
 */
public interface ScaleModeAccessor {

	/**
	 * Retrieves the scale mode from the scale mode property.
	 * 
	 * @return The scale mode stored by the scale mode property.
	 */
	ScaleMode getScaleMode();

	/**
	 * Provides a mutator for a scale mode property.
	 */
	public interface ScaleModeMutator {

		/**
		 * Sets the scale mode for the scale mode property.
		 * 
		 * @param aScaleMode The scale mode to be stored by the scale mode
		 *        property.
		 */
		void setScaleMode( ScaleMode aScaleMode );
	}

	/**
	 * Provides a builder method for a scale mode property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ScaleModeBuilder<B extends ScaleModeBuilder<B>> {

		/**
		 * Sets the scale mode for the scale mode property.
		 * 
		 * @param aScaleMode The scale mode to be stored by the scale mode
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withScaleMode( ScaleMode aScaleMode );
	}

	/**
	 * Provides a scale mode property.
	 */
	public interface ScaleModeProperty extends ScaleModeAccessor, ScaleModeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link ScaleMode}
		 * (setter) as of {@link #setScaleMode(ScaleMode)} and returns the very
		 * same value (getter).
		 * 
		 * @param aScaleMode The {@link ScaleMode} to set (via
		 *        {@link #setScaleMode(ScaleMode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ScaleMode letScaleMode( ScaleMode aScaleMode ) {
			setScaleMode( aScaleMode );
			return aScaleMode;
		}
	}
}
