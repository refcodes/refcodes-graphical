// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a sprite property.
 *
 * @param <T> The sprite's type to be accessed.
 */
public interface SpriteAccessor<T> {

	/**
	 * Retrieves the sprite from the sprite property.
	 * 
	 * @return The sprite stored by the sprite property.
	 */
	T getSprite();

	/**
	 * Provides a mutator for a sprite property.
	 * 
	 * @param <T> The sprite's type to be accessed.
	 */
	public interface SpriteMutator<T> {

		/**
		 * Sets the sprite for the sprite property.
		 * 
		 * @param aSprite The sprite to be stored by the sprite property.
		 */
		void setSprite( T aSprite );
	}

	/**
	 * Provides a builder method for a sprite property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <T> the generic type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface SpriteBuilder<T, B extends SpriteBuilder<T, B>> {

		/**
		 * Sets the sprite for the sprite property.
		 * 
		 * @param aSprite The sprite to be stored by the sprite property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withSprite( T aSprite );
	}

	/**
	 * Provides a sprite property.
	 * 
	 * @param <T> The sprite's type to be accessed.
	 */
	public interface SpriteProperty<T> extends SpriteAccessor<T>, SpriteMutator<T> {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setSprite(Object)} and returns the very same value (getter).
		 * 
		 * @param aSprite The value to set (via {@link #setSprite(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letSprite( T aSprite ) {
			setSprite( aSprite );
			return aSprite;
		}
	}
}
