// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a top padding property.
 */
public interface TopPaddingAccessor {

	/**
	 * Retrieves the top padding from the top padding property.
	 * 
	 * @return The top padding stored by the top padding property.
	 */
	int getTopPadding();

	/**
	 * Provides a mutator for a top padding property.
	 */
	public interface TopPaddingMutator {

		/**
		 * Sets the top padding for the top padding property.
		 * 
		 * @param aTopPadding The top padding to be stored by the top padding
		 *        property.
		 */
		void setTopPadding( int aTopPadding );
	}

	/**
	 * Provides a builder method for a top padding property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TopPaddingBuilder<B extends TopPaddingBuilder<B>> {

		/**
		 * Sets the top padding for the top padding property.
		 * 
		 * @param aTopPadding The top padding to be stored by the top padding
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withTopPadding( int aTopPadding );
	}

	/**
	 * Provides a top padding property.
	 */
	public interface TopPaddingProperty extends TopPaddingAccessor, TopPaddingMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setTopPadding(int)} and returns the very same value (getter).
		 * 
		 * @param aTopPadding The integer to set (via
		 *        {@link #setTopPadding(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letTopPadding( int aTopPadding ) {
			setTopPadding( aTopPadding );
			return aTopPadding;
		}
	}
}
