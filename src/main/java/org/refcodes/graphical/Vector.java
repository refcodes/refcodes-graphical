// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a Y vector property.
 */
public interface Vector extends VectorXAccessor, VectorYAccessor {

	/**
	 * Provides an accessor for a Y vector property.
	 */
	public interface VectorAccessor extends Vector {}

	/**
	 * The Interface VectorMutator.
	 */
	public interface VectorMutator extends VectorXMutator, VectorYMutator {

		/**
		 * Sets the vector.
		 *
		 * @param aVectorX the vector X
		 * @param aVectorY the vector Y
		 */
		void setVector( int aVectorX, int aVectorY );

		/**
		 * Sets the vector.
		 *
		 * @param aVector the new vector
		 */
		void setVector( Vector aVector );
	}

	/**
	 * The Interface VectorBuilder.
	 *
	 * @param <B> the generic type
	 */
	public interface VectorBuilder<B extends VectorBuilder<B>> extends VectorXBuilder<B>, VectorYBuilder<B> {

		/**
		 * With vector.
		 *
		 * @param aVectorX the vector X
		 * @param aVectorY the vector Y
		 * 
		 * @return the b
		 */
		B withVector( int aVectorX, int aVectorY );

		/**
		 * With vector.
		 *
		 * @param aVector the vector
		 * 
		 * @return the b
		 */
		B withVector( Vector aVector );
	}

	/**
	 * The Interface VectorProperty.
	 */
	public interface VectorProperty extends VectorAccessor, VectorMutator, VectorXProperty, VectorYProperty {}

	/**
	 * Equals.
	 *
	 * @param aVectorA the vector A
	 * @param aVectorB the vector B
	 * 
	 * @return true, if successful
	 */
	static boolean equals( Vector aVectorA, Vector aVectorB ) {
		return aVectorA.getVectorX() == aVectorB.getVectorX() && aVectorA.getVectorY() == aVectorB.getVectorY();
	}
}
