// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking eyception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a Y vector property.
 */
public interface VectorYAccessor {

	/**
	 * Retrieves the Y vector from the Y vector property.
	 * 
	 * @return The Y vector stored by the Y vector property.
	 */
	int getVectorY();

	/**
	 * Provides a mutator for a Y vector property.
	 */
	public interface VectorYMutator {

		/**
		 * Sets the Y vector for the Y vector property.
		 * 
		 * @param aVectorY The Y vector to be stored by the Y vector property.
		 */
		void setVectorY( int aVectorY );
	}

	/**
	 * Provides a builder method for a Y vector property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface VectorYBuilder<B extends VectorYBuilder<B>> {

		/**
		 * Sets the Y vector for the Y vector property.
		 * 
		 * @param aVectorY The Y vector to be stored by the Y vector property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withVectorY( int aVectorY );
	}

	/**
	 * Provides a Y vector property.
	 */
	public interface VectorYProperty extends VectorYAccessor, VectorYMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setVectorY(int)} and returns the very same value (getter).
		 * 
		 * @param aVectorY The integer to set (via {@link #setVectorY(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letVectorY( int aVectorY ) {
			setVectorY( aVectorY );
			return aVectorY;
		}
	}
}
