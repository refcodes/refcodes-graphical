// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a {@link ViewportDimension} property.
 */
public interface ViewportDimension extends ViewportHeightAccessor, ViewportWidthAccessor {

	/**
	 * Provides an accessor for a {@link ViewportDimension} property.
	 */
	public interface ViewportDimensionAccessor extends ViewportDimension {}

	/**
	 * The Interface ViewportDimensionMutator.
	 */
	public interface ViewportDimensionMutator extends ViewportHeightMutator, ViewportWidthMutator {

		/**
		 * Sets the viewport dimension.
		 *
		 * @param aWidth the width
		 * @param aHeight the height
		 */
		void setViewportDimension( int aWidth, int aHeight );

		/**
		 * Sets the viewport dimension.
		 *
		 * @param aDimension the new viewport dimension
		 */
		void setViewportDimension( ViewportDimension aDimension );

		/**
		 * Sets the viewport dimension.
		 *
		 * @param aDimension the new viewport dimension
		 */
		void setViewportDimension( Dimension aDimension );
	}

	/**
	 * The Interface ViewportDimensionBuilder.
	 *
	 * @param <B> the generic type
	 */
	public interface ViewportDimensionBuilder<B extends ViewportDimensionBuilder<B>> extends ViewportWidthBuilder<B>, ViewportHeightBuilder<B> {

		/**
		 * With viewport dimension.
		 *
		 * @param aWidth the width
		 * @param aHeight the height
		 * 
		 * @return the b
		 */
		B withViewportDimension( int aWidth, int aHeight );

		/**
		 * With viewport dimension.
		 *
		 * @param aDimension the dimension
		 * 
		 * @return the b
		 */
		B withViewportDimension( ViewportDimension aDimension );

		/**
		 * With viewport dimension.
		 *
		 * @param aDimension the dimension
		 * 
		 * @return the b
		 */
		B withViewportDimension( Dimension aDimension );
	}

	/**
	 * The Interface ViewportDimensionProperty.
	 */
	public interface ViewportDimensionProperty extends ViewportDimensionAccessor, ViewportDimensionMutator, ViewportWidthProperty, ViewportHeightProperty {}

	/**
	 * Equals.
	 *
	 * @param aDimensionA the dimension A
	 * @param aDimensionB the dimension B
	 * 
	 * @return true, if successful
	 */
	static boolean equals( ViewportDimension aDimensionA, ViewportDimension aDimensionB ) {
		return aDimensionA.getViewportWidth() == aDimensionB.getViewportWidth() && aDimensionA.getViewportHeight() == aDimensionB.getViewportHeight();
	}
}
