// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

import org.refcodes.graphical.ViewportDimension.ViewportDimensionBuilder;
import org.refcodes.graphical.ViewportDimension.ViewportDimensionProperty;

/**
 * The Class ViewportDimensionPropertyBuilder.
 */
public class ViewportDimensionPropertyBuilder extends ViewportDimensionImpl implements ViewportDimensionProperty, ViewportDimensionBuilder<ViewportDimensionPropertyBuilder> {

	/**
	 * Instantiates a new viewport dimension property builder impl.
	 */
	public ViewportDimensionPropertyBuilder() {}

	/**
	 * Instantiates a new viewport dimension property builder impl.
	 *
	 * @param aWidth the width
	 * @param aHeight the height
	 */
	public ViewportDimensionPropertyBuilder( int aWidth, int aHeight ) {
		super( aWidth, aHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportWidth( int aWidth ) {
		_width = aWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ViewportDimensionPropertyBuilder withViewportWidth( int aWidth ) {
		_width = aWidth;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportHeight( int aHeight ) {
		_height = aHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ViewportDimensionPropertyBuilder withViewportHeight( int aHeight ) {
		_height = aHeight;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ViewportDimensionPropertyBuilder withViewportDimension( int aWidth, int aHeight ) {
		setViewportDimension( aWidth, aHeight );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ViewportDimensionPropertyBuilder withViewportDimension( ViewportDimension aDimension ) {
		setViewportDimension( aDimension );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportDimension( int aWidth, int aHeight ) {
		_width = aWidth;
		_height = aHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportDimension( ViewportDimension aDimension ) {
		_width = aDimension.getViewportWidth();
		_height = aDimension.getViewportHeight();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ViewportDimensionPropertyBuilder withViewportDimension( Dimension aDimension ) {
		setViewportDimension( aDimension.getWidth(), aDimension.getHeight() );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportDimension( Dimension aDimension ) {
		_width = aDimension.getWidth();
		_height = aDimension.getHeight();
	}
}