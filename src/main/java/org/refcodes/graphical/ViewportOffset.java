// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a Y offset property.
 */
public interface ViewportOffset extends ViewportOffsetXAccessor, ViewportOffsetYAccessor {

	/**
	 * Provides an accessor for a Y offset property.
	 */
	public interface ViewportOffsetAccessor extends ViewportOffset {}

	/**
	 * The Interface ViewportOffsetMutator.
	 */
	public interface ViewportOffsetMutator extends ViewportOffsetXMutator, ViewportOffsetYMutator {

		/**
		 * Sets the viewport offset.
		 *
		 * @param aOffsetX the offset X
		 * @param aOffsetY the offset Y
		 */
		void setViewportOffset( int aOffsetX, int aOffsetY );

		/**
		 * Sets the viewport offset.
		 *
		 * @param aOffset the new viewport offset
		 */
		void setViewportOffset( ViewportOffset aOffset );

		/**
		 * Sets the viewport offset.
		 *
		 * @param aOffset the new viewport offset
		 */
		void setViewportOffset( Offset aOffset );

		/**
		 * Sets the viewport offset.
		 *
		 * @param aOffset the new viewport offset
		 */
		void setViewportOffset( Position aOffset );
	}

	/**
	 * The Interface ViewportOffsetBuilder.
	 *
	 * @param <B> the generic type
	 */
	public interface ViewportOffsetBuilder<B extends ViewportOffsetBuilder<B>> extends ViewportOffsetXBuilder<B>, ViewportOffsetYBuilder<B> {

		/**
		 * With viewport offset.
		 *
		 * @param aOffsetX the offset X
		 * @param aOffsetY the offset Y
		 * 
		 * @return the b
		 */
		B withViewportOffset( int aOffsetX, int aOffsetY );

		/**
		 * With viewport offset.
		 *
		 * @param aOffset the offset
		 * 
		 * @return the b
		 */
		B withViewportOffset( ViewportOffset aOffset );

		/**
		 * With viewport offset.
		 *
		 * @param aOffset the offset
		 * 
		 * @return the b
		 */
		B withViewportOffset( Offset aOffset );

		/**
		 * With viewport offset.
		 *
		 * @param aOffset the offset
		 * 
		 * @return the b
		 */
		B withViewportOffset( Position aOffset );
	}

	/**
	 * The Interface ViewportOffsetProperty.
	 */
	public interface ViewportOffsetProperty extends ViewportOffsetAccessor, ViewportOffsetMutator, ViewportOffsetXProperty, ViewportOffsetYProperty {}

	/**
	 * Equals.
	 *
	 * @param aOffsetA the offset A
	 * @param aOffsetB the offset B
	 * 
	 * @return true, if successful
	 */
	static boolean equals( ViewportOffset aOffsetA, ViewportOffset aOffsetB ) {
		return aOffsetA.getViewportOffsetX() == aOffsetB.getViewportOffsetX() && aOffsetA.getViewportOffsetY() == aOffsetB.getViewportOffsetY();
	}
}
