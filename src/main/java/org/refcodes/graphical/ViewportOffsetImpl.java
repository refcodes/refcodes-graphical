// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * The Class ViewportOffsetImpl.
 *
 * @author steiner
 */
public class ViewportOffsetImpl implements ViewportOffset {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected int _offsetX;

	protected int _offsetY;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new viewport offset impl.
	 */
	protected ViewportOffsetImpl() {}

	/**
	 * Instantiates a new viewport offset impl.
	 *
	 * @param aOffsetX the offset X
	 * @param aOffsetY the offset Y
	 */
	public ViewportOffsetImpl( int aOffsetX, int aOffsetY ) {
		_offsetX = aOffsetX;
		_offsetY = aOffsetY;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getViewportOffsetX() {
		return _offsetX;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getViewportOffsetY() {
		return _offsetY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + _offsetX + ":" + _offsetY + ")@" + hashCode();
	}
}