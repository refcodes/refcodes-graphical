// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a Y offset property.
 */
public interface ViewportOffsetYAccessor {

	/**
	 * Retrieves the Y offset from the Y offset property.
	 * 
	 * @return The Y offset stored by the Y offset property.
	 */
	int getViewportOffsetY();

	/**
	 * Provides a mutator for a Y offset property.
	 */
	public interface ViewportOffsetYMutator {

		/**
		 * Sets the Y offset for the Y offset property.
		 * 
		 * @param aOffsetY The Y offset to be stored by the Y offset property.
		 */
		void setViewportOffsetY( int aOffsetY );
	}

	/**
	 * Provides a builder method for a Y offset property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ViewportOffsetYBuilder<B extends ViewportOffsetYBuilder<B>> {

		/**
		 * Sets the Y offset for the Y offset property.
		 * 
		 * @param aOffsetY The Y offset to be stored by the Y offset property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withViewportOffsetY( int aOffsetY );
	}

	/**
	 * Provides a Y offset property.
	 */
	public interface ViewportOffsetYProperty extends ViewportOffsetYAccessor, ViewportOffsetYMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setViewportOffsetY(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aViewportOffsetY The integer to set (via
		 *        {@link #setViewportOffsetY(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letViewportOffsetY( int aViewportOffsetY ) {
			setViewportOffsetY( aViewportOffsetY );
			return aViewportOffsetY;
		}
	}
}
