// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

/**
 * Provides an accessor for a visible property.
 */
public interface VisibleAccessor {

	/**
	 * Retrieves the visibility from the visible property.
	 * 
	 * @return The visibility stored by the visible property.
	 */
	boolean isVisible();

	/**
	 * Provides a mutator for a visible property.
	 */
	public interface VisibleMutator {

		/**
		 * Sets the visibility for the visible property.
		 * 
		 * @param isVisible The visibility to be stored by the visibility
		 *        property.
		 */
		void setVisible( boolean isVisible );

		/**
		 * Same as setting {@link #setVisible(boolean)} to true.
		 */
		default void show() {
			setVisible( true );
		}

		/**
		 * Same as setting {@link #setVisible(boolean)} to false.
		 */
		default void hide() {
			setVisible( false );
		}
	}

	/**
	 * Provides a builder method for a visible property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface VisibleBuilder<B extends VisibleBuilder<B>> {

		/**
		 * Sets the visibility for the visible property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		default B withShow() {
			return withVisible( true );
		}

		/**
		 * Sets the visibility for the visible property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		default B withHide() {
			return withVisible( false );
		}

		/**
		 * Sets the visibility for the visible property.
		 * 
		 * @param isVisible The visibility to be stored by the visibility
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withVisible( boolean isVisible );
	}

	/**
	 * Provides a visible property.
	 */
	public interface VisibleProperty extends VisibleAccessor, VisibleMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean (setter) as of
		 * {@link #setVisible(boolean)} and returns the very same value
		 * (getter).
		 * 
		 * @param isVisible The boolean to set (via
		 *        {@link #setVisible(boolean)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean letVisible( boolean isVisible ) {
			setVisible( isVisible );
			return isVisible;
		}
	}
}
