// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class BoxBorderModeTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////
	// @formatter:off
	private final boolean [][] BOX_BORDERS = new boolean [][] {
		{ false, false, false, false},
		{ false, false, false,  true},
		{ false, false,  true, false},
		{ false, false,  true,  true},
		{ false,  true, false, false},
		{ false,  true, false,  true},
		{ false,  true,  true, false},
		{ false,  true,  true,  true},
		{  true, false, false, false},
		{  true, false, false,  true},
		{  true, false,  true, false},
		{  true, false,  true,  true},
		{  true,  true, false, false},
		{  true,  true, false,  true},
		{  true,  true,  true, false},
		{  true,  true,  true,  true}
	};
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * Test box borders.
	 */
	@Test
	public void testBoxBorders() {
		String eName;
		BoxBorderMode eBox;
		for ( boolean[] eBorders : BOX_BORDERS ) {
			if ( !eBorders[0] && !eBorders[1] && !eBorders[2] && !eBorders[3] ) {
				eName = "NONE";
			}
			else {
				eName = "";
				if ( eBorders[0] ) {
					eName += "TOP";
				}
				if ( eBorders[1] ) {
					eName += ( eName.length() > 0 ? "_" : "" ) + "RIGHT";
				}
				if ( eBorders[2] ) {
					eName += ( eName.length() > 0 ? "_" : "" ) + "BOTTOM";
				}
				if ( eBorders[3] ) {
					eName += ( eName.length() > 0 ? "_" : "" ) + "LEFT";
				}
			}
			eBox = BoxBorderMode.fromBoxBorders( eBorders[0], eBorders[1], eBorders[2], eBorders[3] );
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "Expecting <" + eName + "> to evaluate to <" + eBox.name() + "> ..." );
			}
			assertEquals( eName, eBox.name() );
		}
	}
}
