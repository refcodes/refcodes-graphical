// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

import static org.junit.jupiter.api.Assertions.*;
import java.awt.Color;
import org.junit.jupiter.api.Test;

public class ColorDepthTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testAwtColorDepth() {
		final Color theAwtColor = new Color( 128, 128, 128 );
		final int theAwtRgb = theAwtColor.getRGB();
		final int theAlpha = ColorDepth.TRUE_COLOR_24_BIT.toAlphaValue( theAwtRgb );
		final int theRed = ColorDepth.TRUE_COLOR_24_BIT.toRedValue( theAwtRgb );
		final int theGreen = ColorDepth.TRUE_COLOR_24_BIT.toGreenValue( theAwtRgb );
		final int theBlue = ColorDepth.TRUE_COLOR_24_BIT.toBlueValue( theAwtRgb );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "ALPHA = " + theAlpha );
			System.out.println( "RED = " + theRed );
			System.out.println( "GREEN = " + theGreen );
			System.out.println( "BLUE = " + theBlue );
		}
		assertEquals( theAlpha, 0 );
		assertEquals( theRed, 128 );
		assertEquals( theGreen, 128 );
		assertEquals( theBlue, 128 );
	}

	@Test
	public void testEdgeCase() {
		final int theGrayColor = 128;
		final int theRgbGrayColor = ColorDepth.TRUE_COLOR_24_BIT.toColor( theGrayColor, ColorDepth.GRAYSCALE_8_BIT );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theGrayColor + " --> " + theRgbGrayColor );
		}
		assertEquals( theRgbGrayColor, 8421504 );
	}
}
