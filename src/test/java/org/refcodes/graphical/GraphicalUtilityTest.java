// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class GraphicalUtilityTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testToGrayScale() {
		int theRgbValue = 0;
		int theGrayValue = GraphicalUtility.toGray( theRgbValue );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theRgbValue + " --> " + theGrayValue );
		}
		assertTrue( theGrayValue <= 255 && theGrayValue >= 0 );
		theRgbValue = 255;
		theGrayValue = GraphicalUtility.toGray( theRgbValue );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theRgbValue + " --> " + theGrayValue );
		}
		assertTrue( theGrayValue <= 255 && theGrayValue >= 0 );
		theRgbValue = Integer.MAX_VALUE;
		theGrayValue = GraphicalUtility.toGray( theRgbValue );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theRgbValue + " --> " + theGrayValue );
		}
		assertTrue( theGrayValue <= 255 && theGrayValue >= 0 );
		theRgbValue = Integer.MAX_VALUE / 2;
		theGrayValue = GraphicalUtility.toGray( theRgbValue );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theRgbValue + " --> " + theGrayValue );
		}
		assertTrue( theGrayValue <= 255 && theGrayValue >= 0 );
		theRgbValue = Integer.MAX_VALUE / 3;
		theGrayValue = GraphicalUtility.toGray( theRgbValue );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theRgbValue + " --> " + theGrayValue );
		}
		assertTrue( theGrayValue <= 255 && theGrayValue >= 0 );
		theRgbValue = Integer.MAX_VALUE / 5;
		theGrayValue = GraphicalUtility.toGray( theRgbValue );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theRgbValue + " --> " + theGrayValue );
		}
		assertTrue( theGrayValue <= 255 && theGrayValue >= 0 );
		theRgbValue = Integer.MIN_VALUE;
		theGrayValue = GraphicalUtility.toGray( theRgbValue );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theRgbValue + " --> " + theGrayValue );
		}
		assertTrue( theGrayValue <= 255 && theGrayValue >= 0 );
		theRgbValue = Integer.MIN_VALUE / 2;
		theGrayValue = GraphicalUtility.toGray( theRgbValue );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theRgbValue + " --> " + theGrayValue );
		}
		assertTrue( theGrayValue <= 255 && theGrayValue >= 0 );
		theRgbValue = Integer.MIN_VALUE / 3;
		theGrayValue = GraphicalUtility.toGray( theRgbValue );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theRgbValue + " --> " + theGrayValue );
		}
		assertTrue( theGrayValue <= 255 && theGrayValue >= 0 );
		theRgbValue = Integer.MIN_VALUE / 5;
		theGrayValue = GraphicalUtility.toGray( theRgbValue );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theRgbValue + " --> " + theGrayValue );
		}
		assertTrue( theGrayValue <= 255 && theGrayValue >= 0 );
	}
}
