// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical;

import java.io.IOException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.data.ext.symbols.SymbolPixmap;
import org.refcodes.data.ext.symbols.SymbolPixmapInputStreamFactory;

public class RgbPixmapImageBuilderTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	@Disabled("Test look and feel of sprite graphics in text mode :-)")
	public void testBoulderDashPixmap() throws IOException {
		final RgbPixmapImageBuilder theBuilder = new RgbPixmapImageBuilder();
		theBuilder.withImageInputStream( new SymbolPixmapInputStreamFactory().create( SymbolPixmap.SKULL ) );
		final RgbPixmap thePixmap = theBuilder.toPixmap();
		String eHexString;
		String eText;
		for ( int y = 0; y < thePixmap.getHeight(); y++ ) {
			eText = "";
			for ( int x = 0; x < thePixmap.getWidth(); x++ ) {
				eHexString = Integer.toHexString( thePixmap.getPixelAt( x, y ).toRgbValue() );
				while ( eHexString.length() < 8 ) {
					eHexString = "0" + eHexString;
				}
				eText += eHexString;
				if ( x < thePixmap.getWidth() ) {
					eText += " ";
				}
			}
			System.out.println( eText );
		}
	}
}
